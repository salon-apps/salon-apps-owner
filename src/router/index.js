import React from 'react';
import { navigationRef } from './root';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
// import Login from '../view/auth/login';
import Intro from '../view/auth/intro';
import MyTabs from './tabs';
import PartnerTabs from './tabsPartner';
import Register from '../view/auth/register';
import Forgot from '../view/auth/forgot';
import Category from '../view/page/category';
import Details from '../view/page/details';
import Order from '../view/page/order';
import Location from '../view/page/location';
import DetailOrder from '../view/page/detailOrder';
import NewOrder from '../view/page/newOrder';
import DetailPartner from '../view/page/detailPartner';
import ChatPage from '../view/page/chat';
import DetailOrderAdmin from '../view/page/detailOrderAdmin';
import SaldoPage from '../view/page/saldo';

const Stack = createStackNavigator();

const MainRoot = () => {

    return (
      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator
          initialRouteName={'Login'}
          headerMode="none"
        >
          <Stack.Screen name="Login" component={Intro} />
          <Stack.Screen name="Tabs" component={MyTabs} />
          <Stack.Screen name="TabsPartner" component={PartnerTabs} />
          <Stack.Screen name="Forgot" component={Forgot} />
          <Stack.Screen name="Category" component={Category} />
          <Stack.Screen name="Details" component={Details} />
          <Stack.Screen name="Order" component={Order} />
          <Stack.Screen name="NewOrder" component={NewOrder} />
          <Stack.Screen name="DetailOrder" component={DetailOrder} />
          <Stack.Screen name="DetailOrderAdmin" component={DetailOrderAdmin} />
          <Stack.Screen name="SelectLoc" component={Location} />
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen name="DetailPartner" component={DetailPartner} />
          <Stack.Screen name="ChatScreen" component={ChatPage} />
          <Stack.Screen name="SaldoPage" component={SaldoPage} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  
}

export default MainRoot;