import React, { useState } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Home from '../view/menu/home';
import Order from '../view/menu/order';
import Help from '../view/menu/help';
import { View, StatusBar, TouchableOpacity, Dimensions, Text } from 'react-native';
import { colors } from '../styles';
import containerStyke from '../styles/container';
import Profile from '../view/menu/profile';
import { waLinking } from '../helper/whatsapp';
import { Badge } from 'react-native-paper';
import { useTracked } from '../service';
import Partner from '../view/menu/partner';
import ListClientPage from '../view/menu/listClient';
import ListOrderClient from '../view/menu/listOrderClient';

const Tab = createMaterialBottomTabNavigator();

function MyTabs() {

  const height = Dimensions.get('screen').height;
  const width = Dimensions.get('screen').width;

  const [state, action] = useTracked();
  const [bar, setBar] = useState(true)

  return (
    <>
      <StatusBar
        barStyle={"light-content"}
        backgroundColor={bar ? colors.primaryD : colors.secondaryD}
      />
      <Tab.Navigator
        activeColor={
          colors.light
        }
        shifting={true}
        barStyle={{
          backgroundColor: colors.secondary
        }}
      >
        <Tab.Screen
          name="TabHome"
          component={Home}
          listeners={{
            tabPress: e => {
              setBar(true)
            },
          }}
          options={{
            tabBarLabel: 'Manage Product',
            tabBarIcon: () => {
              return (
                <Icon
                  name="ios-home"
                  size={30}
                  style={containerStyke.tabsIcon}
                  color={colors.light}
                />
              )
            },
            tabBarColor: colors.primaryD,
          }}
        />
        <Tab.Screen
          name="TabPartner"
          component={Partner}
          listeners={{
            tabPress: e => {
              setBar(false)
            },
          }}
          options={{
            tabBarLabel: 'Manage Partner',
            tabBarIcon: () => {
              return (
                <Icon
                  name="ios-people"
                  size={30}
                  style={containerStyke.tabsIcon}
                  color={colors.light}
                />
              )
            },
            tabBarColor: colors.secondaryD,
          }}
        />
        <Tab.Screen
          name="TabClient"
          component={ListClientPage}
          listeners={{
            tabPress: e => {
              setBar(true)
            },
          }}
          options={{
            tabBarLabel: 'Pengguna',
            tabBarIcon: () => {
              return (
                <Icon
                  name="ios-person"
                  size={30}
                  style={containerStyke.tabsIcon}
                  color={colors.light}
                />
              )
            },
            tabBarColor: colors.primaryD,
          }}
        />
        <Tab.Screen
          name="TabOrderAll"
          component={ListOrderClient}
          listeners={{
            tabPress: e => {
              setBar(false)
            },
          }}
          options={{
            tabBarLabel: 'Transaksi',
            tabBarIcon: () => {
              return (
                <Icon
                  name="ios-cash"
                  size={30}
                  style={containerStyke.tabsIcon}
                  color={colors.light}
                />
              )
            },
            tabBarColor: colors.secondaryD,
          }}
        />
      </Tab.Navigator>
    </>
  );
}

export default MyTabs;