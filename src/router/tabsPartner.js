import React, { useState } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Order from '../view/menu/order';
import Help from '../view/menu/help';
import { View, StatusBar, TouchableOpacity, Dimensions, Text } from 'react-native';
import { colors } from '../styles';
import containerStyke from '../styles/container';
import Profile from '../view/menu/profile';
import { waLinking } from '../helper/whatsapp';
import { Badge } from 'react-native-paper';
import { useTracked } from '../service';

const Tab = createMaterialBottomTabNavigator();

function MyTabs() {

  const height = Dimensions.get('screen').height;
  const width = Dimensions.get('screen').width;

  const [state, action] = useTracked();
  const [bar, setBar] = useState(true)

  return (
    <>
      <StatusBar
        barStyle={"light-content"}
        backgroundColor={bar ? colors.primaryD : colors.secondaryD}
      />
      <Tab.Navigator
        activeColor={
          colors.light
        }
        shifting={true}
        barStyle={{
          backgroundColor: colors.secondary
        }}
      >
        <Tab.Screen
          name="TabOrder"
          component={Order}
          listeners={{
            tabPress: e => {
              setBar(false)
            },
          }}
          options={{
            tabBarLabel: 'Pesanan Masuk',
            tabBarIcon: () => {
              return (
                <View>
                  {
                    state.order ? (
                      <Badge style={{
                        position: 'absolute',
                        zIndex: 1,
                        marginTop: -7,
                        right: -20
                      }}>{
                        state.order.length
                      }</Badge>
                    ) : (
                      null
                    )
                  }
                  <Icon
                    name="md-mail-unread"
                    size={30}
                    style={containerStyke.tabsIcon}
                    color={colors.light}
                  />
                </View>
              )
            },
            tabBarColor: colors.secondaryD
          }}
        />
        <Tab.Screen
          name="TabHelp"
          component={Help}
          listeners={{
            tabPress: e => {
              setBar(true)
            },
          }}
          options={{
            tabBarLabel: 'Bantuan',
            tabBarIcon: () => {
              return (
                <Icon
                  name="ios-help-circle"
                  size={30}
                  style={containerStyke.tabsIcon}
                  color={colors.light}
                />
              )
            },
            tabBarColor: colors.primaryD
          }}
        />
        <Tab.Screen
          name="TabProfile"
          component={Profile}
          listeners={{
            tabPress: e => {
              setBar(false)
            },
          }}
          options={{
            tabBarLabel: 'Profile',
            tabBarIcon: () => {
              return (
                <Icon
                  name="ios-person"
                  size={30}
                  style={containerStyke.tabsIcon}
                  color={colors.light}
                />
              )
            },
            tabBarColor: colors.secondaryD
          }}
        />
      </Tab.Navigator>
    </>
  );
}

export default MyTabs;