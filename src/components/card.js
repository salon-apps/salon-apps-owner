import React from 'react';
import moment from 'moment';
import {
    View,
    TouchableOpacity,
    Image,
    Text,
    Dimensions
} from 'react-native';
import { useTracked } from '../service';
import textStyle from '../styles/text';
import { imageSubCategory, imageCategory } from '../constant/image';
import { DetailLayout, Space } from '../components/containers';
import { colors } from '../styles';
import { List, Button } from 'react-native-paper';
import { api } from '../service/api';
import NotificationSounds, { playSampleSound } from 'react-native-notification-sounds';
import { showMessage } from "react-native-flash-message";
import { waLinking } from '../helper/whatsapp';
import containerStyle from '../styles/container';
import Icon from 'react-native-vector-icons/Ionicons';
import { formatNumber } from '../helper/validator';
import { timeAgo } from '../helper/timeAgo';

export const CardMenu = (props) => {
    return (
        <TouchableOpacity style={{
            alignItems: 'center',
            width: 70,
            padding: 5,
            borderRadius: 15,
            flexWrap: 'nowrap'
        }} onPress={props.onPress}>
            <View style={{
                backgroundColor: colors.lightL,
                borderRadius: 100,
                borderWidth: 1.5,
                borderColor: colors.greyL,
                shadowColor: "#000",
                shadowOffset: {
                    width: 2,
                    height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,

                elevation: 5,
            }}>
                <Image source={props.image} style={{
                    height: 60,
                    width: 60,
                    borderRadius: 100,
                }} />
            </View>
            <View style={{
                paddingTop: 5
            }}>
                <Text style={textStyle.subTitle}>{props.title}</Text>
            </View>
        </TouchableOpacity>
    );
};

export const ListArticle = (props) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={containerStyle.listProduct}>
            <View style={{
                flex: 3,
                padding: 10
            }}>
                <Text style={textStyle.title}>{props.title}</Text>
                <Space size={5} />
                <Text numberOfLines={3} style={textStyle.content}>{props.content}</Text>
            </View>
            <TouchableOpacity style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                padding: 10
            }} onPress={props.update}>
                <View style={{
                    height: 70,
                    width: 70,
                    backgroundColor: colors.primary,
                    borderRadius: 15,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <Icon name="md-settings" size={30} color={colors.light} />
                </View>
            </TouchableOpacity>
        </TouchableOpacity>
    );
};

export const ListTransaction = (props) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={{
                flex: 1,
                marginVertical: 5,
                borderRadius: 15,
                flexDirection: 'row',
                backgroundColor: colors.lightL,
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 1,
                },
                shadowOpacity: 0.20,
                shadowRadius: 1.41,
                elevation: 2
            }}>
            {
                props.distance ? (
                    <View style={{
                        borderRadius: 50,
                        paddingVertical: 5,
                        paddingHorizontal: 10,
                        backgroundColor: colors.primary,
                        position: 'absolute',
                        bottom: 5,
                        right: 5,
                        margin: 2
                    }}>
                        <Text style={[textStyle.subTitle, {
                            color: colors.light,
                            textTransform: 'capitalize'
                        }]}>{props.distance} Km</Text>
                    </View>
                ) : (
                        null
                    )
            }
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                padding: 10
            }}>
                <View style={{
                    height: 70,
                    width: 70,
                    backgroundColor: colors.primaryL,
                    borderRadius: 15,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <Image
                        style={{
                            height: 50,
                            width: 50,
                            borderRadius: 15,
                            resizeMode: 'contain',
                            alignSelf: 'center'
                        }} source={props.icon} />
                </View>
            </View>
            <View style={{
                flex: 3,
                padding: 10
            }}>
                <Text style={textStyle.title}>{props.title}</Text>
                <Space size={5} />
                <Text style={[textStyle.content, {
                    fontSize: 12
                }]}>{props.content}</Text>
                <Text style={[textStyle.subTitle, {
                    fontSize: 12,
                    color: props.idStatus === 5 ? colors.secondary : colors.danger
                }]}>{props.nameStatus}</Text>
            </View>
        </TouchableOpacity>
    );
};

const Stepper = (props) => {

    const active = props.active ? true : false
    const index = props.index ? props.index : 1

    return (
        <View style={{
            flex: 1,
            paddingVertical: 10
        }}>
            <View style={{
                height: 3,
                backgroundColor: active ? colors.primary : colors.greyL,
                width: '100%'
            }} />
            {
                props.first ? (
                    <>
                        <View style={{
                            position: 'absolute',
                            left: 0,
                            top: props.first === 1 ? 0 : 3.5,
                            height: props.first === 1 ? 20 : 15,
                            width: props.first === 1 ? 20 : 15,
                            borderRadius: 100,
                            backgroundColor: colors.primary
                        }} />
                        <View style={{
                            position: 'absolute',
                            top: -20,
                            left: 0
                        }}>
                            <Text style={textStyle.subTitle}>Pending</Text>
                        </View>
                    </>
                ) : (
                        null
                    )
            }
            <View style={{
                position: 'absolute',
                right: 0,
                top: props.now ? 0 : 3.5,
                height: props.now ? 20 : 15,
                width: props.now ? 20 : 15,
                borderRadius: 100,
                backgroundColor: active ? colors.primary : colors.grey,
            }} />
            <View style={{
                position: 'absolute',
                top: index % 2 === 0 ? -20 : 20,
                right: 0
            }}>
                <Text style={textStyle.subTitle}>{props.title}</Text>
            </View>
        </View>
    )
}

export const StepLiner = (props) => {

    const idStatus = props.idStatus ? props.idStatus : 1

    return (
        <View>
            <Space size={30} />
            <View style={{
                flex: 1,
                flexDirection: 'row',
                // backgroundColor: 'grey'
            }}>
                <Stepper
                    title={'Diterima'}
                    first={idStatus}
                    active={idStatus >= 2}
                    index={1}
                    now={idStatus === 2}
                />
                <Stepper
                    title={'Perjalanan'}
                    active={idStatus >= 3}
                    index={2}
                    now={idStatus === 3}
                />
                <Stepper
                    title={'Dilayani'}
                    active={idStatus >= 4}
                    index={3}
                    now={idStatus === 4}
                />
                <Stepper
                    title={'Selesai'}
                    active={idStatus >= 5}
                    index={4}
                    now={idStatus === 5}
                />
            </View>
            <Space size={30} />
            <Text style={[textStyle.welcome, {
                color: colors.secondaryD
            }]}>{props.status}</Text>
            <Text style={[textStyle.content, {
                color: colors.secondaryD,
                fontSize: 12
            }]}>{'Saat ini status pesanan ' + props.status}</Text>
            <Text style={[textStyle.content, {
                color: colors.secondaryD
            }]}>{'Artinya, ' + props.descStatus}</Text>
        </View>
    )
}

export const ListOrder = (props) => {

    const data = props.row;
    const [state, action] = useTracked();

    async function cancelOrder(idOrder) {
        try {
            action({ type: 'loadStart' })
            const cancel = await api('cancelOrder', idOrder)
            console.log(cancel)
            if (cancel.error) {
                action({ type: 'loadStop' })
                action({ type: 'errorAlert', message: cancel.message })
            } else {
                action({ type: 'loadStop' })
                await getDataOrder()
                doNotif(idOrder)
            }
        } catch (error) {
            console.log(error)
            action({ type: 'loadStop' })
            action({ type: 'errorAlert', message: error.message })
        }
    }

    async function getDataOrder() {
        try {
            let dataHistory = []
            let newData = []
            const request = await api('history')
            if (request) {
                if (request.data.length !== 0) {
                    for (let i in request.data) {
                        if (Number(request.data[i].idStatus) >= 5) {
                            dataHistory.push(request.data[i])
                        } else {
                            newData.push(request.data[i])
                        }
                    }
                    action({
                        type: 'setHistory',
                        order: newData,
                        history: dataHistory
                    })
                }
            }
        } catch (error) {
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    function doNotif(id) {
        NotificationSounds.getNotifications().then(soundsList => {
            playSampleSound(soundsList[0]);
        });
        showMessage({
            message: 'Canceled',
            description: 'order id #' + id + ' Telah Berhasil Dibatalkan',
            // onPress: () => {              
            //     RootNavigation.navigate(data.data.page)
            // }
        })
    }

    return (

        <View style={{
            backgroundColor: colors.lightL,
            marginHorizontal: 20,
            marginVertical: 10,
            borderRadius: 7
        }}>
            <View style={{
                borderRadius: 50,
                paddingVertical: 5,
                paddingHorizontal: 10,
                backgroundColor: colors.primary,
                position: 'absolute',
                top: 0,
                right: 0,
                margin: 2
            }}>
                <Text style={[textStyle.subTitle, {
                    color: colors.light,
                    textTransform: 'capitalize'
                }]}>{data.distance} Km</Text>
            </View>
            <List.Accordion
                titleNumberOfLines={2}
                descriptionNumberOfLines={3}
                title={data.nameCategory}
                description={data.nameProduct}
                titleStyle={textStyle.title}
                descriptionStyle={textStyle.subTitle}
                left={() => {
                    return (
                        <View style={{
                            marginHorizontal: 5,
                            height: 70,
                            width: 70,
                            backgroundColor: colors.primaryL,
                            borderRadius: 7,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                            <Image
                                style={{
                                    height: 50,
                                    width: 50,
                                    margin: 5,
                                    padding: 10,
                                    borderRadius: 7,
                                    resizeMode: 'contain',
                                    alignSelf: 'center'
                                }} source={imageSubCategory(data.idProduct)} />
                        </View>
                    )
                }}
            >
                <View style={{
                    paddingHorizontal: 10,
                    marginLeft: -60
                }}>
                    <DetailLayout
                        title={
                            'Detail Pesanan'
                        }>
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            alignItems: 'flex-start',
                            justifyContent: 'flex-start'
                        }}>
                            <View style={{
                                flex: 1
                            }}>
                                <Text style={[textStyle.subTitle, {
                                    fontSize: 12
                                }]}>Produk :</Text>
                                <Text style={[textStyle.subTitle, {
                                    fontSize: 12
                                }]}>Detail Produk :</Text>
                                {
                                    data.nameAddOn ? (
                                        <Text style={[textStyle.subTitle, {
                                            fontSize: 12
                                        }]}>Add On :</Text>
                                    ) : (
                                            null
                                        )
                                }
                                <Text style={[textStyle.subTitle, {
                                    fontSize: 12
                                }]}>Total Pesanan :</Text>
                            </View>
                            <View style={{
                                flex: 1,
                                alignItems: 'flex-end'
                            }}>
                                <Text numberOfLines={1} style={[textStyle.subTitle, {
                                    fontSize: 12,
                                    color: colors.secondaryD
                                }]}>{data.nameProduct}</Text>
                                <Text numberOfLines={1} style={[textStyle.subTitle, {
                                    fontSize: 12,
                                    color: colors.secondaryD
                                }]}>{data.nameDetailProduct}</Text>
                                {
                                    data.nameAddOn ? (
                                        <Text style={[textStyle.subTitle, {
                                            fontSize: 12,
                                            color: colors.secondaryD
                                        }]}>{data.nameAddOn}</Text>
                                    ) : (
                                            null
                                        )
                                }
                                <Text style={[textStyle.subTitle, {
                                    fontSize: 12,
                                    color: colors.secondaryD,
                                    fontWeight: 'bold'
                                }]}>{data.priceAddOn ? formatNumber(data.priceAddOn + data.priceDetailProduct) : formatNumber(data.priceDetailProduct)}</Text>
                            </View>
                        </View>
                    </DetailLayout>
                    <DetailLayout
                        title={
                            'Informasi Pesanan'
                        }>
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            alignItems: 'flex-start',
                            justifyContent: 'flex-start'
                        }}>
                            <View style={{
                                flex: 1
                            }}>
                                <Text style={[textStyle.subTitle, {
                                    fontSize: 12
                                }]}>ID Pesanan :</Text>
                                <Text style={[textStyle.subTitle, {
                                    fontSize: 12
                                }]}>Nama Pemesan :</Text>
                                <Text style={[textStyle.subTitle, {
                                    fontSize: 12
                                }]}>Tanggal Pesanan :</Text>
                                <Text style={[textStyle.subTitle, {
                                    fontSize: 12
                                }]}>Waktu Pesanan :</Text>
                            </View>
                            <View style={{
                                flex: 1,
                                alignItems: 'flex-end'
                            }}>
                                <Text style={[textStyle.subTitle, {
                                    fontSize: 12,
                                    color: colors.secondaryD
                                }]}>{'#' + data.idOrder}</Text>
                                <Text numberOfLines={1} style={[textStyle.subTitle, {
                                    fontSize: 12,
                                    color: colors.secondaryD
                                }]}>{data.fullName}</Text>
                                <Text style={[textStyle.subTitle, {
                                    fontSize: 12,
                                    color: colors.secondaryD
                                }]}>{moment(data.timeOrder).format('DD-MM-YYYY')}</Text>
                                <Text style={[textStyle.subTitle, {
                                    fontSize: 12,
                                    color: colors.secondaryD
                                }]}>{moment(data.timeOrder).format('HH:mm')} WIB</Text>
                            </View>
                        </View>
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            alignItems: 'flex-start',
                            justifyContent: 'flex-start'
                        }}>
                            <View style={{
                                flex: 1
                            }}>
                                <Text style={[textStyle.subTitle, {
                                    fontSize: 12
                                }]}>Lokasi :</Text>
                            </View>
                            <View style={{
                                flex: 1,
                                alignItems: 'flex-end'
                            }}>
                                <Text style={[textStyle.subTitle, {
                                    fontSize: 12,
                                    color: colors.secondaryD,
                                    textAlign: 'right'
                                }]}>{data.detailLoc.display}</Text>
                                <Text style={[textStyle.subTitle, {
                                    fontSize: 12,
                                    color: colors.secondaryD,
                                    textAlign: 'right'
                                }]}>{data.detailLoc.detailLoc}</Text>
                            </View>
                        </View>
                    </DetailLayout>
                </View>
                <Button
                    mode="contained"
                    color={'#00A859'}
                    style={{
                        borderRadius: 0,
                        borderBottomLeftRadius: 7,
                        borderBottomRightRadius: 7
                    }}
                    onPress={props.action}
                    labelStyle={[textStyle.title, {
                        textTransform: 'capitalize',
                        color: colors.light,
                        marginLeft: -50
                    }]}
                >
                    Terima Pesanan
                </Button>
            </List.Accordion>
        </View>
    )
}

export const QandA = (props) => {

    return (
        <View
            key={props.i}
            style={{
                paddingHorizontal: 20
            }}>
            <Text style={[textStyle.subTitle, {
                fontSize: 14
            }]}>{(Number(props.i) + 1) + '. ' + props.title}
            </Text>
            <View style={{
                paddingLeft: 15
            }}>
                <Text style={[textStyle.content, {
                    fontSize: 11
                }]}>{props.content}
                </Text>
            </View>
        </View>
    )
}

export const ChatComponent = ({
    row, myID
}) => {

    return (
        <View style={{
            backgroundColor: row.userID === myID ? colors.primaryL : colors.secondary,
            borderRadius: 100,
            padding: 15,
            paddingVertical: 5,
            margin: 5,
            maxWidth: Dimensions.get('screen').width - 100,
            alignSelf: row.userID === myID ? 'flex-end' : 'flex-start'
        }}>
            <Text style={[textStyle.title, {
                color: row.userID === myID ? colors.dark : colors.light,
                textAlign: row.userID === myID ? 'right' : 'left'
            }]}>{row.value}</Text>
            <View>
                <Text style={[textStyle.subTitle, {
                    color: row.userID === myID ? colors.grey : colors.greyL,
                    textAlign: row.userID === myID ? 'right' : 'left',
                    width: '100%'
                }]}>{timeAgo(row.date)}</Text>
            </View>
        </View>
    )
}

export const RatingComponent = ({
    value
}) => {

    return (

        <View style={[{
            marginTop: 20,
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
            width: '100%'
        }]}>
            <Icon
                name="ios-star"
                size={30}
                style={containerStyle.tabsIcon}
                color={value > 0 ? colors.primary : colors.greyL}
            />
            <Icon
                name="ios-star"
                size={30}
                style={containerStyle.tabsIcon}
                color={value > 1 ? colors.primary : colors.greyL}
            />
            <Icon
                name="ios-star"
                size={30}
                style={containerStyle.tabsIcon}
                color={value > 2 ? colors.primary : colors.greyL}
            />
            <Icon
                name="ios-star"
                size={30}
                style={containerStyle.tabsIcon}
                color={value > 3 ? colors.primary : colors.greyL}
            />
            <Icon
                name="ios-star"
                size={30}
                style={containerStyle.tabsIcon}
                color={value > 4 ? colors.primary : colors.greyL}
            />
        </View>
    )
}

