import React from 'react';
import { View } from 'react-native';
import { SvgXml } from 'react-native-svg';

const secondaryXml =
    `
    <svg
        xmlns="http://www.w3.org/2000/svg"
        width="600"
        height="599"
        fillRule="evenodd"
        clipRule="evenodd"
        imageRendering="optimizeQuality"
        shapeRendering="geometricPrecision"
        textRendering="geometricPrecision"
        viewBox="0 0 599.52 598.94"
    >
        <defs>
            <clipPath id="a">
                <path d="M0 0h599.52v598.94H0V0z"></path>
            </clipPath>
        </defs>
        <g clip-path="url(#a)">
            <circle cx="274.59" cy="72.38" r="473.09" fill="#BBDEFB"></circle>
            <circle cx="299.76" cy="49.3" r="473.09" fill="#2196F3"></circle>
        </g>
        <path fill="none" d="M0 0L599.52 0 599.52 598.94 0 598.94z"></path>
    </svg>
`;

const mainXml =
`
    <svg
        xmlns="http://www.w3.org/2000/svg"
        width="600"
        height="599"
        fillRule="evenodd"
        clipRule="evenodd"
        imageRendering="optimizeQuality"
        shapeRendering="geometricPrecision"
        textRendering="geometricPrecision"
        viewBox="0 0 599.52 598.94"
    >
        <defs>
            <clipPath id="a">
                <path d="M0 0h599.52v598.94H0V0z"></path>
            </clipPath>
        </defs>
        <g clip-path="url(#a)">
            <circle cx="274.59" cy="72.38" r="473.09" fill="#F8BBD0"></circle>
            <circle cx="299.76" cy="49.3" r="473.09" fill="#EC407A"></circle>
        </g>
        <path fill="none" d="M0 0L599.52 0 599.52 598.94 0 598.94z"></path>
    </svg>
    `;

export const MainBG = () => {
    return (
        <View style={{
            position: 'absolute',
            width: '100%',
            height: '100%'
        }}>
            <View style={{
                marginTop: '-50%'
            }}>
                <SvgXml xml={mainXml} width="120%" />
            </View>
        </View>
    );
}

export const SecondaryBG = () => {
    return (
        <View style={{
            position: 'absolute',
            width: '100%',
            height: '100%'
        }}>
            <View style={{
                marginTop: '-50%'
            }}>
                <SvgXml xml={secondaryXml} width="120%" />
            </View>
        </View>
    );
}