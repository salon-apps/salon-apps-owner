import React from 'react';
import { Dialog } from 'react-native-paper';

export const CustomDialog = ({
    open,
    setOpen,
    title,
    children
}) => {

    return (
        <Dialog
            visible={open}
            onDismiss={() => {
                setOpen(false)
            }}>
            <Dialog.Title>{title}</Dialog.Title>
            <Dialog.Content>
                {
                    children
                }
            </Dialog.Content>
        </Dialog>
    )
}