import React, { useState } from 'react';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment'
import {
    View,
    TextInput,
    Text,
    TouchableOpacity
} from 'react-native';
import { IconButton } from 'react-native-paper';
import { colors } from '../styles';
import { emailValidator } from '../helper/validator';
import inputStyle from '../styles/input';
import textStyle from '../styles/text';
import possStyle from '../styles/position';
import containerStyle from '../styles/container';
import { Space } from './containers';

export const Input = (props) => {

    const [focus, setFocus] = useState(false);
    const [error, setError] = useState(false);
    const [errorText, setErrorText] = useState('');
    const [hidePass, setHidePass] = useState(true);

    const value = props.value;
    const action = props.action;
    const title = props.title;
    const type = props.type ? props.type : false;
    const lineColor = error ? colors.danger : focus ? colors.primary : colors.greyL;
    const keyboardType = props.keyboardType ? props.keyboardType : null;
    const onBlur = props.onBlur ? props.onBlur : false

    function validator() {
        if (type) {
            if (type === 'email') {
                setError(!emailValidator(value));
                setErrorText('masukan email dengan benar')
                if (value.length === 0) {
                    setError(false)
                }
            } else if (type === 'general') {
                if (value.length < 5) {
                    setError(true);
                    setErrorText('masukan data dengan benar')
                    if (value.length === 0) {
                        setError(false)
                    }
                } else {
                    setError(false)
                }
            }
        }
    }

    function handleBlur() {
        validator()
        setFocus(false);
    }

    return (
        <View style={inputStyle.inputContainer}>
            {
                value.length === 0 ? (
                    <View />
                ) : (
                        <Text style={textStyle.titleInput}>{title}</Text>
                    )
            }
            <TextInput
                onFocus={() => {
                    setFocus(true)
                }}
                onBlur={onBlur ? onBlur : () => {
                    handleBlur();
                }}
                secureTextEntry={type === 'password' ? hidePass : false}
                keyboardType={keyboardType}
                value={value}
                onChangeText={action}
                style={inputStyle.imputText}
                placeholder={title}
                multiline={props.multiline ? props.multiline : false}
            />
            {error ? (
                <Text style={inputStyle.warningInput}>{errorText}</Text>
            ) : (
                    <View />
                )}
            {type === 'password' ? (
                <View style={possStyle.absoluteRight}>
                    <IconButton
                        icon={hidePass ? 'eye-off' : 'eye'}
                        color={colors.dark}
                        size={30}
                        animated
                        onPress={() => {
                            setHidePass(!hidePass)
                        }}
                    />
                </View>
            ) : (
                    <View />
                )}
            <View style={{
                width: '100%',
                height: 1,
                marginTop: value.length === 0 ? 10 : 5,
                backgroundColor: lineColor
            }} />
        </View>
    );
};

export const DateTimeInput = (props) => {


    return (
        <>
            <View
                style={{
                    paddingHorizontal: 10
                }}>
                <View style={[containerStyle.flexBetwen]}>
                    <View>
                        <Text style={[textStyle.title, {
                            color: colors.grey
                        }]}>Waktu Pemesanan</Text>
                        <Text style={[textStyle.content, {
                            color: colors.grey,
                            fontSize: 13,
                            marginLeft: 10
                        }]}>{moment(props.value).format('DD-MM-YYYY HH:mm:ss')}</Text>
                    </View>
                    <View style={containerStyle.flexRight}>
                        <IconButton
                            icon="calendar"
                            color={colors.secondary}
                            size={25}
                            onPress={props.dateOpen}
                        />
                        <IconButton
                            icon="av-timer"
                            color={colors.secondary}
                            size={25}
                            onPress={props.timeOpen}
                        />
                    </View>
                </View>
                <Space size={10} />
                <View style={{
                    height: 1,
                    width: '100%',
                    backgroundColor: colors.greyL
                }} />
            </View>
            {props.payload.show && (
                <DateTimePicker
                    // minimumDate={new Date(moment().add(2, 'h'))}
                    testID="dateTimePicker"
                    timeZoneOffsetInMinutes={0}
                    value={props.value}
                    mode={props.payload.mode}
                    is24Hour={true}
                    display="default"
                    onChange={props.action}
                />
            )}
        </>
    )
}