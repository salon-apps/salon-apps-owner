import React from 'react';
import { SliderBox } from "react-native-image-slider-box";
import image from '../constant/image';
import { colors } from '../styles';
import { View } from 'react-native';

const banner = [
    image.slide1,
    image.slide2,
    image.slide3,
    image.slide4,
]

export const Banner = ({ render }) => {
    return (
        render ? (
        <SliderBox
            images={banner}
            sliderBoxHeight={170}
            // onCurrentImagePressed={index => console.log(`image ${index} pressed`)}
            dotColor={colors.primary}
            inactiveDotColor={colors.greyL}
            dotStyle={{
                width: 30,
                height: 10,
                borderRadius: 15,
                padding: 0,
                margin: 0,
                marginLeft: -15
            }}
            autoplay
            circleLoop
            ImageComponentStyle={{
                borderRadius: 15,
                width: '96%',
                marginTop: 5,
            }}
        /> ) : (
            <View style={{
                height: 170,
                borderRadius: 15,
                width: '96%',
                marginTop: 5,
                marginLeft: 7,
                backgroundColor: colors.greyL
            }} />
        )
    );
};