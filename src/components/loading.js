import React from 'react';
import { View } from 'react-native';
import { colors } from '../styles';

export const OrderSkelleton = () => {

    return (
        <View style={{
            marginHorizontal: 20
        }}>
            {
                [1, 2, 3].map((row, i) => (
                    <View key={i}
                        style={{
                            width: '100%',
                            height: 100,
                            backgroundColor: colors.greyL,
                            borderRadius: 7,
                            marginVertical: 10                                                
                        }} />
                ))
            }
        </View>
    )
}