import React from 'react';
import { StyleSheet } from 'react-native';
import { colors } from '.';
import font from '../constant/font';

const styles = StyleSheet.create({
    inputContainer: {
        backgroundColor: colors.lightL,
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    imputText: {
        fontFamily: font.primary,
        paddingHorizontal: 10,
        paddingVertical: 0,
        color: colors.grey,
    },
    warningInput: {
        fontSize: 10,
        color: colors.danger,
        fontFamily: font.primary
    }
});

export default styles;