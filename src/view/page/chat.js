import React, { useState, useEffect } from 'react';
import { useTracked } from '../../service';
import { Space, MainHeaderContainer, DetailLayout } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    Image,
    TextInput,
    Keyboard,
    Dimensions
} from 'react-native';
import { IconButton } from 'react-native-paper';
import { MainBG } from '../../components/abstrack';
import { colors } from '../../styles';
import containerStyle from '../../styles/container';
import textStyle from '../../styles/text';
import database from '@react-native-firebase/database';
import moment from 'moment';
import { ChatComponent } from '../../components/card';

const ChatPage = ({ navigation, route: {
    params: {
        chatID,
        myID
    }
} }) => {

    const [state, action] = useTracked();
    const [chat, setChat] = useState([]);
    const [message, setMessage] = useState('');

    useEffect(() => {
        action({
            type: 'loadStart'
        })
        action({
            type: 'loadStop'
        })
        const onValueChange = database()
            .ref(`/chat/${chatID}/`)
            .on('value', data => {
                let snapshot = JSON.parse(JSON.stringify(data));
                if (snapshot) {
                    let newSnapshot = [];
                    let keys = Object.keys(snapshot);
                    for (let row of keys) {
                        newSnapshot.unshift(snapshot[row])
                    }
                    setChat(newSnapshot)
                }
            });

        // Stop listening for updates when no longer required
        return () =>
            database()
                .ref(`/chat/${chatID}/`)
                .off('value', onValueChange);
    }, []);

    function sendChat() {
        const newReference = database()
            .ref(`/chat/${chatID}/`)
            .push();

        console.log('Auto generated key: ', newReference.key);

        newReference
            .set({
                userID: myID,
                date: (new Date()).getTime(),
                value: message
            })
            .then(() => {
                setMessage('');
                Keyboard.dismiss();
            });
    }

    return (
        <MainHeaderContainer
            dark={false}
            back
            barColor={colors.primaryD}
            title={`Chat Order #${chatID}`}
            nav={navigation}>
            <MainBG />
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    background: 'transparent',
                    padding: 10
                }}
            >
                {
                    chat.length > 0 ? chat.map((row, i) => (
                        <View key={i.toString()}>
                            <ChatComponent index={i} row={row} myID={myID} />
                        </View>
                    )) : (
                            null
                        )
                }
            </ScrollView>
            <View style={
                {
                    padding: 10,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    backgroundColor: colors.primary
                }
            }>
                <View style={{
                    paddingLeft: 10,
                    flex: 5,
                    backgroundColor: colors.light,
                    borderRadius: 100,
                }}>
                    <TextInput placeholder={'Tulis isi chat disini'} value={message} onChangeText={(text) => {
                        setMessage(text)
                    }} style={textStyle.title} />
                </View>
                <View style={{
                    flex: 1,
                }}>
                    <IconButton
                        icon={'send'}
                        color={colors.light}
                        size={40}
                        style={{
                            margin: 0,
                            marginLeft: 5
                        }}
                        animated
                        onPress={() => {
                            sendChat()
                        }}
                    />
                </View>
            </View>
        </MainHeaderContainer>
    );
};

export default ChatPage;
