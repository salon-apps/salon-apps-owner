import React, { useState, useEffect } from 'react';
import moment from 'moment';
import { useTracked } from '../../service';
import { api } from '../../service/api';
import { MainHeaderContainer, DetailLayout, Space } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
} from 'react-native';
import MapView from 'react-native-maps';
import textStyle from '../../styles/text';
import containerStyle from '../../styles/container';
import { colors } from '../../styles';
import { Button } from 'react-native-paper';
import { Input, DateTimeInput } from '../../components/input';
import { formatNumber, lengthValidator } from '../../helper/validator';
import { getProfile } from '../../helper/enct';

const Order = ({ navigation, route: {
    params: {
        payload
    }
} }) => {

    const [state, action] = useTracked();
    const [detailLoc, setDetailLoc] = useState('')
    const [note, setNote] = useState('')
    const [time, setTime] = useState({
        dateTime: new Date(moment().add(2, 'h')),
        show: false,
        mode: 'date'
    })

    useEffect(() => {
        getLoc()
    }, []);

    async function getLoc() {
        const profile = await getProfile();
        const location = profile.location;
        if (location.idLoc) {
            const direction = {
                display: location.display,
                latitude: Number(location.latitude),
                longitude: Number(location.longitude)
            }
            setDetailLoc(location.detailLoc)
            action({
                type: 'locSelect',
                data: direction
            })
        }
    }

    async function doStore(payload) {
        try {
            action({ type: 'loadStart' })
            const product = payload.detail.product;
            const addOn = payload.detail.addOn;
            const data = {
                idDetailProduct: product.idDetailProduct,
                idAddOn: addOn.idAddOn ? addOn.idAddOn : null,
                note: payload.note,
                timeOrder: payload.time,
                location: {
                    display: payload.locSelect.display,
                    latitude: payload.locSelect.latitude,
                    longitude: payload.locSelect.longitude,
                    detailLoc: payload.detailLoc
                }
            }
            const request = await api('order', data)
            if (request) {
                action({ type: 'loadStop' })
                if (request.error) {
                    action({ type: 'errorAlert', message: error.message })
                } else {
                    action({ type: 'successAlert', message: 'Transaksi Berhasil Dibuat' })
                    action({
                        type: 'setTabsNavigator',
                        data: 'TabOrder'
                    })
                    navigation.reset({
                        index: 0,
                        routes: [{ name: 'Tabs' }],
                    });
                }
            }
        } catch (error) {
            console.log(error)
            action({ type: 'loadStop' })
        }
    }

    return (
        <MainHeaderContainer
            title={'Konfirmasi Pesanan'}
            dark={false}
            back
            barColor={colors.primaryD}
            nav={navigation}>
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <DetailLayout
                    title={
                        'Lokasi'
                    }>
                    <Text style={textStyle.content}>
                        {state.locSelect ? state.locSelect.display : 'Lokasi Belum Dipilih'}
                    </Text>
                </DetailLayout>
                <View style={{
                    height: 150,
                    width: '100%',
                    backgroundColor: colors.grey
                }}>
                    {state.locSelect ? (
                        <MapView
                            style={{
                                flex: 1
                            }}
                            initialRegion={{
                                latitude: state.locSelect.latitude,
                                longitude: state.locSelect.longitude,
                                latitudeDelta: 0.01,
                                longitudeDelta: 0.01,
                            }}
                        >
                            <MapView.Marker
                                coordinate={{
                                    latitude: state.locSelect.latitude,
                                    longitude: state.locSelect.longitude
                                }}
                                draggable={false}
                            />
                        </MapView>
                    ) : null}
                </View>
                <Button
                    style={{
                        borderRadius: 0
                    }}
                    icon="map"
                    mode="contained"
                    color={colors.grey}
                    onPress={() => {
                        navigation.navigate('SelectLoc');
                    }}>
                    {state.locSelect ? 'Ubah Lokasi' : 'Pilih Lokasi'}
                </Button>
                <View style={{
                    padding: 20,
                    backgroundColor: colors.lightL
                }}>
                    <Input
                        title={'Detail Lokasi'}
                        type={'general'}
                        value={detailLoc}
                        action={text => {
                            setDetailLoc(text)
                        }}
                    />
                    <Input
                        title={'Catatan Pelanggan'}
                        type={'general'}
                        value={note}
                        action={text => {
                            setNote(text)
                        }}
                    />
                    <DateTimeInput
                        value={time.dateTime}
                        payload={time}
                        dateOpen={() => {
                            const newTime = { ...time }
                            newTime.show = true
                            newTime.mode = 'date'
                            setTime(newTime)
                        }}
                        timeOpen={() => {
                            const newTime = { ...time }
                            newTime.show = true
                            newTime.mode = 'time'
                            setTime(newTime)
                        }}
                        action={(event, selectedDate) => {
                            const currentDate = selectedDate || time.dateTime;
                            // const currentDate = selectedDate;                            
                            if (moment(currentDate).isAfter(moment().add(1, 'h'))) {
                                const newTime = { ...time }
                                newTime.dateTime = new Date(currentDate)
                                newTime.show = false
                                setTime(newTime);
                                console.log(newTime)
                            } else {
                                const newTime = { ...time }
                                newTime.show = false
                                setTime(newTime);
                                console.log(newTime)
                                action({ type: 'errorAlert', message: 'Waktu Pemesanan Minimal 1 Jam Saat Ini' })
                            }
                        }} />
                </View>
                <DetailLayout
                    title={
                        'Detail Pesanan'
                    }>
                    <Space size={5} />
                    <Text style={[textStyle.title, {
                        fontSize: 14,
                        color: colors.secondary
                    }]}>
                        {payload.product.title}
                    </Text>
                    <View style={containerStyle.flexBetwen}>
                        <Text style={[textStyle.content, {
                            fontSize: 12,
                            color: colors.grey
                        }]}>
                            {payload.product.nameDetailProduct}
                        </Text>
                        <Text style={[textStyle.title, {
                            color: colors.secondary
                        }]}>
                            {formatNumber(payload.product.priceDetailProduct)}
                        </Text>
                    </View>
                    {
                        payload.addOn ? (
                            <View style={containerStyle.flexBetwen}>
                                <Text style={[textStyle.content, {
                                    fontSize: 12,
                                    color: colors.grey
                                }]}>
                                    {payload.addOn.nameAddOn}
                                </Text>
                                <Text style={[textStyle.title, {
                                    color: colors.secondary
                                }]}>
                                    {formatNumber(payload.addOn.priceAddOn)}
                                </Text>
                            </View>
                        ) : (
                                null
                            )
                    }
                    <Space size={10} />
                    <View style={{
                        height: 2,
                        width: '100%',
                        backgroundColor: colors.greyL
                    }} />
                    <View style={containerStyle.flexBetwen}>
                        <Text style={[textStyle.content, {
                            fontSize: 12,
                            color: colors.grey
                        }]}>
                            {'Total'}
                        </Text>
                        <Text style={[textStyle.title, {
                            color: colors.secondary
                        }]}>
                            {payload.addOn ? formatNumber(payload.product.priceDetailProduct + payload.addOn.priceAddOn) : formatNumber(payload.product.priceDetailProduct)}
                        </Text>
                    </View>
                </DetailLayout>
                <Space size={20} />
                <Button
                    style={{
                        borderRadius: 0,
                        paddingVertical: 5
                    }}
                    mode="contained"
                    color={colors.primaryD}
                    onPress={() => {
                        const data = {
                            locSelect: state.locSelect,
                            detailLoc,
                            note,
                            time: time.dateTime,
                            detail: payload
                        }
                        if (state.locSelect) {
                            if (lengthValidator({ detailLoc, note }, 5)) {
                                doStore(data)
                            } else {
                                action({ type: 'errorAlert', message: 'Masukan Data Dengan Benar' })
                            }
                        } else {
                            action({ type: 'errorAlert', message: 'Pilih Lokasi Terlebih Dahulu' })
                        }
                    }}>
                    Pesan
                </Button>
            </ScrollView>
        </MainHeaderContainer>
    );
};

export default Order;
