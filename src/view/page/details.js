import React, { useState, useEffect } from 'react';
import { useTracked } from '../../service';
import { Space, MainHeaderContainer, DetailLayout } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import { Button } from 'react-native-paper';
import { MainBG } from '../../components/abstrack';
import { str } from '../../constant/string';
import { colors } from '../../styles';
import containerStyle from '../../styles/container';
import textStyle from '../../styles/text';
import { CardMenu, ListArticle } from '../../components/card';
import image from '../../constant/image';
import { Banner } from '../../components/banner';
import { ListPrice, ButtonEditList } from '../../components/button';
import { formatNumber } from '../../helper/validator';
import { Input } from '../../components/input';
import { api } from '../../service/api';
import { onChange } from 'react-native-reanimated';

const Details = ({ navigation, route: {
    params: {
        title,
        payload,
        ID
    }
} }) => {

    const [state, action] = useTracked();
    const [priceTogle, setPriceTogle] = useState('');
    const [price, setPrice] = useState(0);
    const [addOnTogle, setAddOnTogle] = useState('');
    const [addOn, setAddOn] = useState(0);
    const [descTogle, setDescTogle] = useState(false);
    const [desc, setDesc] = useState('');
    const [product, setProduct] = useState([]);
    const [changed, setChanged] = useState(false);
    const [load, setLoad] = useState(false);

    useEffect(() => {
        getData()
    }, []);

    async function getData() {
        try {
            setLoad(true)
            const request = await api('detailProduct', ID)
            if (request.data) {
                setLoad(false)
                setDesc(request.data.descProduct)
                setProduct(request.data.detailProduct)
            }
        } catch (error) {
            setLoad(false)
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    async function updateAddOnPrice() {
        try {
            action({ type: 'loadStart' })
            const request = await api('updateAddOnPrice', {
                idAddOn: addOnTogle,
                priceAddOn: addOn
            })

            if (request) {
                action({ type: 'loadStop' })
                if (request.error) {
                    action({ type: 'errorAlert', message: request.message })
                } else {
                    getData()
                    action({ type: 'successAlert', message: 'Sukses Ubah Data' })
                }
            }
        } catch (error) {
            action({ type: 'errorAlert', message: error.message })
            action({ type: 'loadStop' })
        }
    }

    async function updateDescProduct() {
        try {
            action({ type: 'loadStart' })

            const request = await api('updateDescProduct', {
                idProduct: ID,
                descProduct: desc
            })

            if (request) {
                action({ type: 'loadStop' })
                if (request.error) {
                    action({ type: 'errorAlert', message: request.message })
                } else {
                    getData()
                    action({ type: 'successAlert', message: 'Sukses Ubah Data' })
                }
            }
        } catch (error) {
            action({ type: 'errorAlert', message: error.message })
            action({ type: 'loadStop' })
        }
    }

    async function updateDetailPrice() {
        try {
            action({ type: 'loadStart' })
            const request = await api('updateDetailPrice', {
                idDetailProduct: priceTogle,
                priceDetailProduct: price
            })
            if (request) {
                action({ type: 'loadStop' })
                if (request.error) {
                    action({ type: 'errorAlert', message: request.message })
                } else {
                    getData()
                    action({ type: 'successAlert', message: 'Sukses Ubah Data' })
                }
            }
        } catch (error) {
            action({ type: 'errorAlert', message: error.message })
            action({ type: 'loadStop' })
        }
    }

    return (
        <MainHeaderContainer
            dark={false}
            back
            barColor={colors.primaryD}
            title={'Ubah Data Produk'}
            nav={navigation}>
            <MainBG />
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'space-between',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <View>
                    <DetailLayout
                        title={
                            'Nama Produk'
                        }>
                        <Text style={textStyle.content}>{
                            title
                        }</Text>
                        <ButtonEditList onPress={() => {
                            action({
                                type: 'errorAlert',
                                message: 'ROLE Tidak Diijinkan Update Nama Produk'
                            })
                        }} />
                    </DetailLayout>
                    <DetailLayout
                        title={
                            'Deskripsi'
                        }>
                        {
                            descTogle ? (
                                <View style={{
                                    marginVertical: 20
                                }}>
                                    <Input
                                        title={'Ubah Deskripsi'}
                                        value={desc}
                                        multiline
                                        onBlur={() => {
                                            if (changed) {
                                                updateDescProduct()
                                            }
                                            setChanged(false)
                                        }}
                                        action={text => {
                                            setChanged(true)
                                            setDesc(text)
                                        }}
                                    />
                                </View>
                            ) : (
                                    <Text style={textStyle.content}>{
                                        desc
                                    }</Text>
                                )
                        }
                        <ButtonEditList onPress={() => {
                            setDescTogle(!descTogle)
                        }} />
                    </DetailLayout>
                    <DetailLayout title={
                        'Ubah ' + payload.displayPrice
                    }>
                        <View style={{
                            marginHorizontal: 10
                        }}>
                            {
                                product.map((row, i) => (
                                    <View key={i}>
                                        <DetailLayout
                                            title={
                                                row.nameDetailProduct
                                            }>
                                            {
                                                priceTogle === row.idDetailProduct ? (
                                                    <Input
                                                        title={'Ubah Harga'}
                                                        keyboardType={'numeric'}
                                                        value={String(price)}
                                                        onBlur={() => {
                                                            if (changed) {
                                                                updateDetailPrice()
                                                            }
                                                            setChanged(false)
                                                        }}
                                                        action={text => {
                                                            setChanged(true)
                                                            setPrice(text)
                                                        }}
                                                    />
                                                ) : (
                                                        <Text style={textStyle.price}>{row.priceDetailProduct}</Text>
                                                    )
                                            }
                                            <ButtonEditList
                                                title={'ubah harga'}
                                                onPress={() => {
                                                    if (row.idDetailProduct === priceTogle) {
                                                        setPriceTogle('')
                                                    } else {
                                                        setPrice(row.priceDetailProduct)
                                                        setPriceTogle(row.idDetailProduct)
                                                    }
                                                }} />
                                            <Space size={30} />
                                            {
                                                row.addOn[0].idAddOn ? (
                                                    <>
                                                        <Text tyle={textStyle.title}>Add On {row.nameDetailProduct}</Text>
                                                        <View style={{
                                                            paddingLeft: 10
                                                        }}>
                                                            {
                                                                row.addOn.map((jRow, j) => (
                                                                    <View key={j}>
                                                                        <DetailLayout
                                                                            title={
                                                                                jRow.nameAddOn
                                                                            }>
                                                                            {
                                                                                addOnTogle === jRow.idAddOn ? (
                                                                                    <Input
                                                                                        title={'Ubah ' + jRow.nameAddOn}
                                                                                        keyboardType={'numeric'}
                                                                                        value={String(addOn)}
                                                                                        onBlur={() => {
                                                                                            if (changed) {
                                                                                                updateAddOnPrice()
                                                                                            }
                                                                                            setChanged(false)
                                                                                        }}
                                                                                        action={text => {
                                                                                            setChanged(true)
                                                                                            setAddOn(text)
                                                                                        }}
                                                                                    />
                                                                                ) : (
                                                                                        <Text style={textStyle.price}>{jRow.priceAddOn}</Text>
                                                                                    )
                                                                            }
                                                                            <ButtonEditList
                                                                                title={'ubah Addon'}
                                                                                onPress={() => {
                                                                                    if (jRow.idAddOn === addOnTogle) {
                                                                                        setAddOnTogle('')
                                                                                    } else {
                                                                                        setAddOn(jRow.priceAddOn)
                                                                                        setAddOnTogle(jRow.idAddOn)
                                                                                    }
                                                                                }} />
                                                                        </DetailLayout>
                                                                    </View>
                                                                ))
                                                            }
                                                        </View>
                                                    </>
                                                ) : (
                                                        null
                                                    )
                                            }
                                        </DetailLayout>
                                    </View>
                                ))
                            }
                        </View>

                    </DetailLayout>
                </View>
            </ScrollView>
        </MainHeaderContainer>
    );
};

export default Details;
