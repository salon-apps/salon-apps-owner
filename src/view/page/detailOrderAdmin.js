import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { MainHeaderContainer, DetailLayout, Space } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    Alert,
    Image
} from 'react-native';
import { Button } from 'react-native-paper';
import MapView from 'react-native-maps';
import textStyle from '../../styles/text';
import containerStyle from '../../styles/container';
import { colors } from '../../styles';
import { formatNumber } from '../../helper/validator';
import CountDown from 'react-native-countdown-component';
import GetLocation from 'react-native-get-location';
import getDirections from 'react-native-google-maps-directions';
import { useTracked } from '../../service';
import { api } from '../../service/api';
import AsyncStorage from '@react-native-community/async-storage';
import { jwtVerify } from '../../helper/enct';

const DetailOrderAdmin = ({ navigation, route: {
    params: {
        data
    }
} }) => {

    const [state, action] = useTracked();

    function doRejected() {
        try {
            Alert.alert(
                "Rejected",
                "Apakah anda benar benar akan mereject transaksi ini ?",
                [
                    {
                        text: "Tidak",
                        onPress: () => {
                            console.log('close')
                        },
                        style: "cancel"
                    },
                    {
                        text: "Iya", onPress: () => {
                            startReject()
                        }
                    }
                ],
                { cancelable: false })
        } catch (error) {
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    async function startReject() {
        try {
            let request = await api('forceRejectOrder', data.idOrder)
            if (!request.error) {
                action({
                    type: 'updateState',
                    data: 'updateTransaksi'
                })
                action({ type: 'successAlert', message: 'Berhasil Force Cancel Order' })
            } else {
                action({ type: 'errorAlert', message: request.message })
            }
        } catch (error) {
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    async function doChat() {
        const userProfile = await jwtVerify(await AsyncStorage.getItem('token'));
        navigation.navigate('ChatScreen', {
            chatID: data.idOrder,
            myID: userProfile.payload.idPartner
        });
    }

    return (
        <MainHeaderContainer
            title={'Detail Pesanan'}
            dark={false}
            back
            barColor={colors.primaryD}
            nav={navigation}>
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <DetailLayout
                    title={
                        'Informasi Pelanggan'
                    }>
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'flex-start'
                    }}>
                        <Image
                            style={{
                                height: 70,
                                width: 70,
                                marginRight: 20,
                                marginTop: 20,
                                padding: 10,
                                borderRadius: 100,
                                resizeMode: 'contain',
                                alignSelf: 'center'
                            }} source={{ uri: data.userImage }} />
                        <View style={{
                            marginTop: 20,
                        }}>
                            <Text numberOfLines={1} style={[textStyle.welcome, {
                                color: colors.primary
                            }]}>{data.fullName}</Text>
                            {
                                data.idStatus === 2 || data.idStatus === 3 || data.idStatus === 4 ? (
                                    <Button
                                        style={{
                                            marginTop: -5
                                        }}
                                        icon="android-messages"
                                        mode="text"
                                        color={colors.secondary}
                                        onPress={() => {
                                            doChat()
                                        }}>
                                        Kirim Pesan
                                    </Button>
                                ) : (
                                        null
                                    )
                            }
                        </View>
                    </View>
                </DetailLayout>
                <DetailLayout
                    title={
                        'Lokasi'
                    }>
                    <Text style={textStyle.content}>
                        {data.detailLoc.display}
                    </Text>
                </DetailLayout>
                <View style={{
                    height: 150,
                    width: '100%',
                    backgroundColor: colors.grey
                }}>
                    <MapView
                        style={{
                            flex: 1
                        }}
                        initialRegion={{
                            latitude: data.detailLoc.latitude,
                            longitude: data.detailLoc.longitude,
                            latitudeDelta: 0.01,
                            longitudeDelta: 0.01
                        }}
                    >
                        <MapView.Marker
                            coordinate={{
                                latitude: data.detailLoc.latitude,
                                longitude: data.detailLoc.longitude
                            }}
                            draggable={false}
                        />
                    </MapView>
                </View>
                <View style={{
                    padding: 20,
                    backgroundColor: colors.lightL
                }}>
                    <DetailLayout
                        title={
                            'Detail Lokasi'
                        }>
                        <Text style={textStyle.content}>
                            {data.detailLoc.detailLoc}
                        </Text>
                    </DetailLayout>
                    <DetailLayout
                        title={
                            'Catatan'
                        }>
                        <Text style={textStyle.content}>
                            {data.note}
                        </Text>
                    </DetailLayout>
                    <DetailLayout
                        title={
                            'Waktu Pemesanan'
                        }>
                        <Text style={textStyle.content}>
                            {moment(data.timeOrder).format('DD-MM-SS HH:mm:ss') + ' WIB'}
                        </Text>
                    </DetailLayout>

                </View>
                <DetailLayout
                    title={
                        'Detail Pesanan'
                    }>
                    <Space size={5} />
                    <Text style={[textStyle.title, {
                        fontSize: 12,
                        color: colors.secondary
                    }]}>
                        {data.title}
                    </Text>
                    <View style={containerStyle.flexBetwen}>
                        <Text style={[textStyle.content, {
                            fontSize: 12,
                            color: colors.grey
                        }]}>
                            {data.nameProduct}
                        </Text>
                        <Text style={[textStyle.title, {
                            color: colors.secondary
                        }]}>
                            {formatNumber(data.priceDetailProduct)}
                        </Text>
                    </View>
                    {
                        data.priceAddOn ? (
                            <View style={containerStyle.flexBetwen}>
                                <Text style={[textStyle.content, {
                                    fontSize: 12,
                                    color: colors.grey
                                }]}>
                                    {data.nameAddOn}
                                </Text>
                                <Text style={[textStyle.title, {
                                    color: colors.secondary
                                }]}>
                                    {formatNumber(data.priceAddOn)}
                                </Text>
                            </View>
                        ) : (
                                null
                            )
                    }
                    <Space size={10} />
                    <View style={{
                        height: 2,
                        width: '100%',
                        backgroundColor: colors.greyL
                    }} />
                    <View style={containerStyle.flexBetwen}>
                        <Text style={[textStyle.content, {
                            fontSize: 12,
                            color: colors.grey
                        }]}>
                            {'Total'}
                        </Text>
                        <Text style={[textStyle.title, {
                            color: colors.secondary
                        }]}>
                            {formatNumber(
                                data.priceAddOn ? data.priceAddOn + data.priceDetailProduct : data.priceDetailProduct
                            )}
                        </Text>
                    </View>
                </DetailLayout>
                <Space size={100} />
            </ScrollView>
            <View style={{
                position: 'absolute',
                bottom: 0,
                width: '100%',
                padding: 20,
            }}>
                {
                    data.idStatus === 2 || data.idStatus === 3 || data.idStatus === 4 ? (
                        <Button
                            color={colors.primary}
                            style={{
                                paddingVertical: 5,
                                borderRadius: 10,
                                flex: 1,
                                marginRight: 10
                            }}
                            mode="contained"
                            onPress={() => {
                                doRejected()
                            }}>
                            Force Cancel Order
                        </Button>
                    ) : (
                            null
                        )
                }
                <View style={containerStyle.flexCenter}>
                </View>
            </View>
        </MainHeaderContainer>
    );
};

export default DetailOrderAdmin;
