import React, { useState, useEffect } from 'react';
import MapView from 'react-native-maps';
import GetLocation from 'react-native-get-location';
// import Geocoder from 'react-native-geocoding';
import { useTracked } from '../../service';
import textStyle from '../../styles/text';
import { Button } from 'react-native-paper';
import { MainHeaderContainer } from '../../components/containers';
import {
    ScrollView,
    View,
    Dimensions,
    Image,
    Text
} from 'react-native';
import { MainBG } from '../../components/abstrack';
import { colors } from '../../styles';
import containerStyle from '../../styles/container';
import { ListArticle } from '../../components/card';
import image from '../../constant/image';
import { key } from '../../constant/key';
import { api } from '../../service/api';

const Location = ({ navigation }) => {

    const [state, action] = useTracked();
    const [region, setRegion] = useState({
        latitude: -6.21462,
        longitude: 106.84513,
        latitudeDelta: 1,
        longitudeDelta: 1
    })
    const [mark, setMark] = useState({
        latitude: -6.21462,
        longitude: 106.84513,
        display: 'Jakarta, Indonesia'
    })

    useEffect(() => {
        getMyLocation()
    }, []);

    async function geocoding(lat, long) {
        const data = await api('geocode', {
            key: key.locationiq,
            latitude: lat,
            longitude: long
        })
        if (data) {
            const direction = {
                display: data.display_name,
                latitude: lat,
                longitude: long,
            }
            setMark(direction)
            action({
                type: 'locSelect',
                data: direction
            })
            action({ type: 'loadStop' });            
        }
    }

    function getMyLocation() {
        action({ type: 'loadStart' });
        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 15000,
        })
            .then(location => {
                const newRegion = {
                    latitude: location.latitude,
                    longitude: location.longitude,
                    latitudeDelta: 0.001,
                    longitudeDelta: 0.001
                };
                geocoding(location.latitude, location.longitude)
                setRegion(newRegion)
            })
            .catch(error => {
                const { code, message } = error;
                console.warn(code, message);
            })
    }

    return (
        <MainHeaderContainer
            title={'Pilih Lokasi'}
            dark={false}
            back
            barColor={colors.primaryD}
            nav={navigation}>
            <MainBG />
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <View style={{
                    height: '100%',
                    width: '100%'
                }}>
                    <MapView
                        onPress={({ nativeEvent }) => {
                            const latitude = nativeEvent.coordinate.latitude;
                            const longitude = nativeEvent.coordinate.longitude;
                            geocoding(latitude, longitude)
                        }}
                        showsUserLocation
                        // followUserLocation
                        zoomEnabled
                        initialRegion={region}
                        region={region}
                        style={{ flex: 1 }} //window pake Dimensions
                        onRegionChangeComplete={(e) => {
                            setRegion({
                                latitude: e.latitude,
                                longitude: e.longitude,
                                latitudeDelta: e.latitudeDelta,
                                longitudeDelta: e.longitudeDelta
                            })
                        }}>
                        <MapView.Marker
                            coordinate={{
                                latitude: mark.latitude,
                                longitude: mark.longitude,
                            }}
                            draggable={false}
                            title="Lokasi"
                            description="Lokasi Kamu Saat Ini" />
                    </MapView>
                    <View style={{
                        position: 'absolute',
                        padding: 20,
                        width: (Dimensions.get('window').width) - 20,
                        backgroundColor: 'rgba(0,0,0,0.4)',
                        bottom: 10,
                        left: 10,
                        borderRadius: 7
                    }}>
                        <View style={containerStyle.flexBetwen}>
                            <View style={{
                                flex: 1
                            }}>
                                <Text style={[
                                    textStyle.title,
                                    {
                                        color: colors.light
                                    }
                                ]}>Lokasi</Text>
                                <Text style={[
                                    textStyle.content,
                                    {
                                        color: colors.light
                                    }
                                ]}>{mark.display}</Text>
                            </View>
                            <View>
                                <Button
                                    // icon="arrow-right-bold"
                                    mode="contained"
                                    color={colors.secondary}
                                    onPress={() => {
                                        navigation.goBack()
                                    }}>
                                    OK
                                </Button>
                            </View>
                        </View>
                    </View>
                </View>
                {/* <GooglePlacesInput />    */}
            </ScrollView>
        </MainHeaderContainer>
    );
};

export default Location;
