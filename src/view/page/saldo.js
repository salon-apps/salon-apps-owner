import React, { useState, useEffect } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { useTracked } from '../../service';
import { Space, MainHeaderContainer, DetailLayout } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import { SecondaryBG } from '../../components/abstrack';
import { IconButton, Button } from 'react-native-paper'
import { colors } from '../../styles';
import textStyle from '../../styles/text';
import containerStyle from '../../styles/container';
import { QandA } from '../../components/card';
import { api } from '../../service/api';
import { waLinking } from '../../helper/whatsapp';
import { key } from '../../constant/key';
import { Input } from '../../components/input';
import { formatNumber } from '../../helper/validator';

const SaldoPage = ({ navigation, route: {
    params: {
        profile
    }
} }) => {

    const [state, dispatch] = useTracked();
    const [saldo, setSaldo] = useState(0);
    const [about, setAbout] = useState(false);
    const [check, setCheck] = useState(false);

    const height = Dimensions.get('screen').height;
    const width = Dimensions.get('screen').width;

    async function getSettings() {
        const response = await api('about');
        if (!response.error) {
            if (response.data.settings) {
                console.log(profile)
                setAbout(response.data.settings);
            }
        }
    }

    useEffect(() => {
        getSettings()
    }, []);

    return (
        <MainHeaderContainer
            dark={false}
            back
            barColor={colors.secondaryD}
            title={'Isi Saldo'}
            nav={navigation}>
            <SecondaryBG />
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <DetailLayout
                    title={
                        'Apa itu saldo ?'
                    }>
                    <Text style={textStyle.content}>
                        {`Saldo adalah tanda jasa kamu kepada jelita agar kamu tetap dapat notifikasi pesanan. Berapa jasa yang harus kamu bayarkan kepada jelita ? saat ini dengan pesentase ${about ? about.commission : 'loading'}% dari total pesanan yang kamu terima. Dan Saldo akan otomatis terpotong setelah pesanan selesai`}
                    </Text>
                </DetailLayout>
                <DetailLayout
                    title={
                        'Bagaimana cara isi saldo ?'
                    }>
                    <Text style={textStyle.content}>
                        {'Silakan kamu isi berapa saldo yang ingin kamu isi lalu transfer ke salah satu nomor rekening dibawah ini komplit sampai digit terakhir untuk mempermudah konfirmasi kami'}
                    </Text>
                </DetailLayout>
                <Input
                    title={'Isi Saldo'}
                    value={saldo}
                    keyboardType={'number-pad'}
                    action={text => {
                        setSaldo(text)
                    }}
                />
                <DetailLayout
                    title={
                        'Pilih Rekening ini'
                    }>
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'flex-start',
                        alignItems: 'flex-start',
                        padding: 10
                    }}>

                        {
                            about ? (
                                about.rekening ? (
                                    about.rekening.map((row, i) => (
                                        <TouchableOpacity
                                            key={i.toString()}
                                            style={{
                                                flexDirection: 'row',
                                                justifyContent: 'flex-start',
                                                alignItems: 'center',
                                                backgroundColor: check === i ? colors.primaryL : colors.greyL,
                                                width: '100%',
                                                marginTop: 10,
                                                padding: 10
                                            }}
                                            onPress={() => {
                                                setCheck(i)
                                            }}>
                                            <IconButton
                                                icon={check === i ? 'checkbox-marked-outline' : 'checkbox-blank-outline'}
                                                color={check === i ? colors.primary : colors.grey}
                                                size={30}
                                                animated
                                                onPress={() => {
                                                    setCheck(i)
                                                }}
                                            />
                                            <View>
                                                <Text style={textStyle.title}>{row.norek}</Text>
                                                <Text style={textStyle.subTitle}>{row.bank}</Text>
                                                <Text style={textStyle.subTitle}>{row.name}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    ))
                                ) : (null)
                            ) : (null)
                        }
                    </View>
                </DetailLayout>
                {
                    check && saldo > 10000 ? (
                        <View style={{
                            borderRadius: 15,
                            borderWidth: 3,
                            borderColor: colors.primary,
                            padding: 5,
                            margin: 20,
                            borderStyle: 'dotted',
                            backgroundColor: colors.light
                        }}>
                        <DetailLayout
                            title={
                                'Konfirmasi Isi Saldo'
                            }>
                            <Text style={textStyle.content} selectable>
                                Nomor Rekening "{
                                    about.rekening[check].norek
                                }"
                            </Text>
                            <Text style={textStyle.content} selectable>
                                Bank "{
                                    about.rekening[check].bank
                                }"
                            </Text>
                            <Text style={textStyle.content} selectable>
                                Atas Nama "{
                                    about.rekening[check].name
                                }"
                            </Text>
                            <Space size={10} />
                            <Text style={textStyle.title}>
                                Total {formatNumber(saldo + profile.idPartner)}
                            </Text>
                            <Space size={10} />
                            <Text style={textStyle.content}>Note: </Text>
                            <Text>Bayar lengkap hingga digit terakhir untuk mempermudah konfirmasi pembayaran anda</Text>
                        </DetailLayout>
                        </View>
                    ) : (null)
                }
                <Space size={50} />
            </ScrollView>
            {
                saldo > 10000 ? (
                    <View style={{
                        position: 'absolute',
                        zIndex: 2,
                        bottom: 0,
                        padding: 5,
                        width: '100%'
                    }}>
                        <View style={{
                            height: 2,
                            width: '100%',
                            backgroundColor: colors.greyL
                        }} />                       
                        <View style={containerStyle.flexBetwen}>
                            <Button
                                mode={'contained'}
                                color={colors.primary}
                                style={{
                                    flex: 1
                                }}
                                onPress={() => {
                                    waLinking(`Hai Jelita Salon & Spa Homeservices saya ${profile.fullName} ingin mengkonfirmasi pembayaran sejumlah ${formatNumber(saldo + profile.idPartner)} ke Rekening ${about.rekening[check].norek} (${about.rekening[check].name})`, key.jelitaPhone)
                                }}>
                                Konfirmasi Pembayaran
                        </Button>
                        </View>
                    </View>
                ) : (null)
            }
        </MainHeaderContainer>
    );
};

export default SaldoPage;
