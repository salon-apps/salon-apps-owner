import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { MainHeaderContainer, DetailLayout, Space } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    Alert,
    Image
} from 'react-native';
import { Button } from 'react-native-paper';
import MapView from 'react-native-maps';
import textStyle from '../../styles/text';
import containerStyle from '../../styles/container';
import { colors } from '../../styles';
import { formatNumber } from '../../helper/validator';
import CountDown from 'react-native-countdown-component';
import GetLocation from 'react-native-get-location';
import getDirections from 'react-native-google-maps-directions';
import { useTracked } from '../../service';
import { api } from '../../service/api';
import AsyncStorage from '@react-native-community/async-storage';
import { jwtVerify } from '../../helper/enct';

const DetailOrder = ({ navigation, route: {
    params: {
        data
    }
} }) => {

    const [state, action] = useTracked();
    const [cd, setCd] = useState(false)
    const [flag, setFlag] = useState(0)

    useEffect(() => {
        if (data) {
            if (data.idStatus !== 5) {
                if (data.idStatus === 4) {
                    setFlag(2)
                } else {
                    calculateDay()
                }
            } else {
                setFlag(3)
            }
        }
    }, [data])

    function calculateDay() {
        if (moment().isAfter(moment(data.timeOrder).add(1, 'days'))) {
            if (data.idStatus !== 6 && data.idStatus !== 7) {
                setFlag(-1)
                Alert.alert(
                    "Expired",
                    "Pesanan Telah Berakhir, Apakah kamu telah selesai melaksanakan pesanan ini ?",
                    [
                        {
                            text: "Tidak",
                            onPress: () => {
                                Alert.alert(
                                    "Rejected",
                                    "Apakah anda benar benar tidak menjalankan pesanan ini ?",
                                    [
                                        {
                                            text: "Tidak",
                                            onPress: () => {
                                                console.log('close')
                                            },
                                            style: "cancel"
                                        },
                                        {
                                            text: "Iya", onPress: () => {
                                                doRejected()
                                                setFlag(3)
                                            }
                                        }
                                    ],
                                    { cancelable: false })
                            },
                            style: "cancel"
                        },
                        {
                            text: "Iya", onPress: () => {
                                setFlag(3)
                                finishOrder()
                            }
                        }
                    ],
                    { cancelable: false })
            } else {
                setFlag(3)
            }
        } else {
            let date = moment(data.timeOrder).subtract(2, 'hours');
            if (moment().isBefore(moment(date))) {
                let date1 = new Date(date);
                let date2 = new Date();
                let ms = Math.abs(date1 - date2);
                let sec = parseInt(Math.floor(ms / 1000));
                setCd(sec)
            } else {
                setCd(1)
            }
        }
    }

    async function doProsess() {
        try {
            let request = await api('prosesOrder', data.idOrder)
            if (!request.error) {
                action({
                    type: 'updateState',
                    data: 'getOrder'
                })
                action({ type: 'successAlert', message: 'Selamat Menikmati Perjalanan Anda' })
            } else {
                action({ type: 'errorAlert', message: request.message })
            }
        } catch (error) {
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    async function doRejected() {
        try {
            let request = await api('rejectOrder', data.idOrder)
            if (!request.error) {
                action({ type: 'successAlert', message: 'Kami berharap anda akan lebih serius dalam melayani pelanggan' })
            } else {
                action({ type: 'errorAlert', message: request.message })
            }
        } catch (error) {
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    async function doWorked() {
        action({ type: 'loadStart' })
        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 15000,
        })
            .then(location => {
                worked(location.latitude, location.longitude)
            })
            .catch(error => {
                console.log(error.message)
                worked(-6.200000, 106.816666)
            })
    }

    async function worked(
        latitude,
        longitude
    ) {
        try {
            let request = await api('workedOrder', {
                idOrder: data.idOrder,
                latitude,
                longitude
            })
            if (request) {
                action({ type: 'loadStop' })
                if (!request.error) {
                    setFlag(2)
                    action({
                        type: 'updateState',
                        data: 'getOrder'
                    })
                    action({ type: 'successAlert', message: 'Beri Layanan Terbaik Untuk Pelanggan' })
                } else {
                    action({ type: 'errorAlert', message: 'Pastikan Kamu Berada Ditujuan Terlebih Dahulu' })
                }
            }
        } catch (error) {
            action({ type: 'loadStop' })
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    async function finishOrder() {
        try {
            action({
                type: 'loadStart'
            })
            let request = await api('finishOrderForce', data.idOrder)
            if (request) {
                action({
                    type: 'loadStop'
                })
                action({
                    type: 'updateState',
                    data: 'getorder'
                })
                if (!request.error) {
                    setFlag(3)
                    action({ type: 'successAlert', message: 'terimakasih telah menjadi mirta kami' })
                } else {
                    action({ type: 'errorAlert', message: request.message })
                }
            }
        } catch (error) {
            action({
                type: 'loadStop'
            })
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    function openMaps() {
        if (data.idStatus === 2) {
            doProsess()
        }
        action({
            type: 'loadStart'
        })
        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 15000,
        })
            .then(location => {
                handleGetDirections(location.latitude, location.longitude)
            })
            .catch(error => {
                console.log(error.message)
                // action({ type: 'errorAlert', message: error.message })        
                handleGetDirections(-6.200000, 106.816666)
            })
    }

    function handleGetDirections({
        latitude,
        longitude
    }) {

        const payload = {
            source: {
                latitude,
                longitude
            },
            destination: {
                latitude: data.detailLoc.latitude,
                longitude: data.detailLoc.longitude
            },
            params: [
                {
                    key: "travelmode",
                    value: "driving"        // may be "walking", "bicycling" or "transit" as well
                },
                {
                    key: "dir_action",
                    value: "navigate"       // this instantly initializes navigation using the given travel mode
                }
            ],
            waypoints: [
            ]
        }

        action({
            type: 'loadStop'
        })

        getDirections(payload)
    }

    async function doChat() {
        const userProfile = await jwtVerify(await AsyncStorage.getItem('token'));
        navigation.navigate('ChatScreen', {
            chatID: data.idOrder,
            myID: userProfile.payload.idPartner
        });
    }

    return (
        <MainHeaderContainer
            title={'Detail Pesanan'}
            dark={false}
            back
            barColor={colors.primaryD}
            nav={navigation}>
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <DetailLayout
                    title={
                        'Informasi Pelanggan'
                    }>
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'flex-start'
                    }}>
                        <Image
                            style={{
                                height: 70,
                                width: 70,
                                marginRight: 20,
                                marginTop: 20,
                                padding: 10,
                                borderRadius: 100,
                                resizeMode: 'contain',
                                alignSelf: 'center'
                            }} source={{ uri: data.userImage }} />
                        <View style={{
                            marginTop: 20,
                        }}>
                            <Text numberOfLines={1} style={[textStyle.welcome, {
                                color: colors.primary
                            }]}>{data.fullName}</Text>
                            {
                                data.idStatus === 2 || data.idStatus === 3 || data.idStatus === 4 ? (
                                    <Button
                                        style={{
                                            marginTop: -5
                                        }}
                                        icon="android-messages"
                                        mode="text"
                                        color={colors.secondary}
                                        onPress={() => {
                                            doChat()
                                        }}>
                                        Kirim Pesan
                                    </Button>
                                ) : (
                                        null
                                    )
                            }
                        </View>
                    </View>
                </DetailLayout>
                <DetailLayout
                    title={
                        'Lokasi'
                    }>
                    <Text style={textStyle.content}>
                        {data.detailLoc.display}
                    </Text>
                </DetailLayout>
                <View style={{
                    height: 150,
                    width: '100%',
                    backgroundColor: colors.grey
                }}>
                    <MapView
                        style={{
                            flex: 1
                        }}
                        initialRegion={{
                            latitude: data.detailLoc.latitude,
                            longitude: data.detailLoc.longitude,
                            latitudeDelta: 0.01,
                            longitudeDelta: 0.01
                        }}
                    >
                        <MapView.Marker
                            coordinate={{
                                latitude: data.detailLoc.latitude,
                                longitude: data.detailLoc.longitude
                            }}
                            draggable={false}
                        />
                    </MapView>
                </View>
                <View style={{
                    padding: 20,
                    backgroundColor: colors.lightL
                }}>
                    <DetailLayout
                        title={
                            'Detail Lokasi'
                        }>
                        <Text style={textStyle.content}>
                            {data.detailLoc.detailLoc}
                        </Text>
                    </DetailLayout>
                    <DetailLayout
                        title={
                            'Catatan'
                        }>
                        <Text style={textStyle.content}>
                            {data.note}
                        </Text>
                    </DetailLayout>
                    <DetailLayout
                        title={
                            'Waktu Pemesanan'
                        }>
                        <Text style={textStyle.content}>
                            {moment(data.timeOrder).format('DD-MM-SS HH:mm:ss') + ' WIB'}
                        </Text>
                    </DetailLayout>

                </View>
                {
                    cd ? (
                        <DetailLayout
                            title={
                                'Sisa Waktu Proses Pesanan'
                            }>
                            <View style={{
                                marginTop: 20
                            }}>
                                {
                                    flag === 1 ? (
                                        <Text style={[textStyle.title, {
                                            textAlign: 'center',
                                            paddingBottom: 20,
                                            color: colors.secondary
                                        }]}>
                                            Sudah Saatnya menjemput pelanggan
                                        </Text>
                                    ) : (
                                            <Text style={[textStyle.title, {
                                                textAlign: 'center',
                                                paddingBottom: 20,
                                                color: colors.primary
                                            }]}>
                                                (2 jam sebelum keberangkatan)
                                            </Text>
                                        )
                                }
                                <CountDown
                                    until={cd}
                                    onFinish={() => {
                                        setFlag(1)
                                    }}
                                    // onPress={() => alert('hello')}
                                    size={20}
                                />
                            </View>
                        </DetailLayout>
                    ) : (
                            null
                        )
                }
                <DetailLayout
                    title={
                        'Detail Pesanan'
                    }>
                    <Space size={5} />
                    <Text style={[textStyle.title, {
                        fontSize: 12,
                        color: colors.secondary
                    }]}>
                        {data.title}
                    </Text>
                    <View style={containerStyle.flexBetwen}>
                        <Text style={[textStyle.content, {
                            fontSize: 12,
                            color: colors.grey
                        }]}>
                            {data.nameProduct}
                        </Text>
                        <Text style={[textStyle.title, {
                            color: colors.secondary
                        }]}>
                            {formatNumber(data.priceDetailProduct)}
                        </Text>
                    </View>
                    {
                        data.priceAddOn ? (
                            <View style={containerStyle.flexBetwen}>
                                <Text style={[textStyle.content, {
                                    fontSize: 12,
                                    color: colors.grey
                                }]}>
                                    {data.nameAddOn}
                                </Text>
                                <Text style={[textStyle.title, {
                                    color: colors.secondary
                                }]}>
                                    {formatNumber(data.priceAddOn)}
                                </Text>
                            </View>
                        ) : (
                                null
                            )
                    }
                    <Space size={10} />
                    <View style={{
                        height: 2,
                        width: '100%',
                        backgroundColor: colors.greyL
                    }} />
                    <View style={containerStyle.flexBetwen}>
                        <Text style={[textStyle.content, {
                            fontSize: 12,
                            color: colors.grey
                        }]}>
                            {'Total'}
                        </Text>
                        <Text style={[textStyle.title, {
                            color: colors.secondary
                        }]}>
                            {formatNumber(
                                data.priceAddOn ? data.priceAddOn + data.priceDetailProduct : data.priceDetailProduct
                            )}
                        </Text>
                    </View>
                </DetailLayout>
                <Space size={100} />
            </ScrollView>
            <View style={{
                position: 'absolute',
                bottom: 0,
                width: '100%',
                padding: 20,
            }}>
                <View style={containerStyle.flexCenter}>
                    {
                        flag === 1 ? (
                            <Button
                                color={colors.secondary}
                                style={{
                                    paddingVertical: 5,
                                    borderRadius: 10,
                                    flex: 1,
                                    marginRight: 10
                                }}
                                mode="contained"
                                onPress={() => {
                                    doWorked()
                                }}>
                                Sampai
                            </Button>
                        ) : (
                                null
                            )
                    }
                    {
                        flag !== 3 ? (
                            <Button
                                color={colors.primary}
                                style={{
                                    paddingVertical: 5,
                                    borderRadius: 10,
                                    flex: 5
                                }}
                                disabled={flag <= 0}
                                mode="contained"
                                onPress={() => {
                                    if (flag === 1) {
                                        openMaps()
                                    } else if (flag === 2) {
                                        finishOrder()
                                    }
                                }}>
                                {
                                    flag === 1 ? 'Berangkat Ketujuan' :
                                        flag === 2 ? 'Layanan Selesai' :
                                            flag === -1 ? 'Pesanan Expired' :
                                                'Berangkat Ketujuan'
                                }
                            </Button>
                        ) : (
                                null
                            )
                    }
                </View>
            </View>
        </MainHeaderContainer>
    );
};

export default DetailOrder;
