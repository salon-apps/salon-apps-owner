import React, { useEffect, useCallback, useState } from 'react';
import moment from 'moment';
import { CountdownCircleTimer } from 'react-native-countdown-circle-timer';
import { MainHeaderContainer, DetailLayout, Space } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    Image,
    BackHandler,
    Animated
} from 'react-native';
import { Button } from 'react-native-paper';
import MapView from 'react-native-maps';
import textStyle from '../../styles/text';
import containerStyle from '../../styles/container';
import { colors } from '../../styles';
import { formatNumber } from '../../helper/validator';
import CountDown from 'react-native-countdown-component';
import GetLocation from 'react-native-get-location';
import getDirections from 'react-native-google-maps-directions';
import { useTracked } from '../../service';
import { useFocusEffect } from '@react-navigation/native';
import { api } from '../../service/api';

const NewOrder = ({ navigation, route: {
    params
} }) => {

    const [state, action] = useTracked();
    const [actions, setAction] = useState(false);
    const [data, setData] = useState({
        "idOrder": null,
        "note": "",
        "timeOrder": new Date(),
        "detailLoc": {
            "display": "Loading",
            "latitude": -6.2140663,
            "longitude": 106.8397779,
            "detailLoc": "Loading"
        },
        "hp": null,
        "fullName": "",
        "nameCategory": "",
        "idProduct": null,
        "nameProduct": "",
        "nameDetailProduct": "",
        "priceDetailProduct": 0,
        "nameAddOn": "",
        "priceAddOn": 0,
        "idStatus": null,
        "nameStatus": "pending",
        "descStatus": ""
    });

    async function getData(ID) {
        try {
            action({
                type: 'loadStart'
            })
            const request = await api('getOrder', ID)
            if (request) {
                action({
                    type: 'loadStop'
                })
                if (request.data) {
                    setData(request.data)
                }
            }
        } catch (error) {
            action({
                type: 'loadStop'
            })
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    useEffect(() => {
        getData(params.params)
    }, [])

    useFocusEffect(
        useCallback(() => {
            const onBackPress = () => {
                return true
            };

            BackHandler.addEventListener('hardwareBackPress', onBackPress);

            return () =>
                BackHandler.removeEventListener('hardwareBackPress', onBackPress);
        }, []),
    );

    async function doReject() {
        try {       
            setAction(true)
            const id = data.idOrder;
            action({
                type: 'loadStart'
            })
            let request = await api('newRejectOrder', id)
            navigation.goBack()
            if (request) {   
                action({
                    type: 'loadStop'
                })
                action({ type: 'successAlert', message: 'sayang sekali anda telah menolak pesanan ini' })                                    
            }
        } catch (error) {  
            navigation.goBack()
            action({
                type: 'loadStop'
            })                  
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    async function getDataLoc(latitude, longitude) {
        try {            
            let request = await api('order', {
                latitude, 
                longitude
            })
            if (request) {                
                if (request.data.length !== 0) {
                    action({
                        type: 'setHistory',
                        order: request.data,
                        history: state.history
                    })
                    navigation.goBack()                    
                }
            }
        } catch (error) {            
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    async function doAccept(id) {
        try {       
            setAction(true)
            action({
                type: 'loadStart'
            })
            let request = await api('acceptOrder', id)
            if (request) {   
                action({
                    type: 'loadStop'
                })             
                navigation.goBack()                    
                if (!request.error) {
                    action({ type: 'successAlert', message: 'sukses menerima order' })                    
                    action({
                        type: 'updateState',
                        data: 'TabHistory'
                    })
                } else {                    
                    action({
                        type: 'updateState',
                        data: 'TabHistory'
                    })
                    action({ type: 'errorAlert', message: request.message })                    
                }
            }
        } catch (error) {     
            navigation.goBack()
            action({
                type: 'loadStop'
            })                  
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    return (
        <MainHeaderContainer
            title={'Pesanan Baru'}
            dark={false}
            back={actions}
            barColor={colors.primaryD}
            nav={navigation}>
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <DetailLayout
                    title={
                        'Informasi Pelanggan'
                    }>
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'flex-start'
                    }}>
                        <Image
                            style={{
                                height: 70,
                                width: 70,
                                marginRight: 20,
                                marginTop: 20,
                                padding: 10,
                                borderRadius: 100,
                                resizeMode: 'contain',
                                alignSelf: 'center'
                            }} source={{ uri: data.userImage }} />
                        <View style={{
                            marginTop: 20,
                        }}>
                            <Text numberOfLines={1} style={[textStyle.welcome, {
                                color: colors.primary
                            }]}>{data.fullName}</Text>                            
                        </View>
                    </View>
                </DetailLayout>
                <DetailLayout
                    title={
                        'Lokasi'
                    }>
                    <Text style={textStyle.content}>
                        {data.detailLoc.display}
                    </Text>
                </DetailLayout>
                <View style={{
                    height: 150,
                    width: '100%',
                    backgroundColor: colors.grey
                }}>
                    <MapView
                        style={{
                            flex: 1
                        }}
                        initialRegion={{
                            latitude: -6.117664,
                            longitude: 106.906349,
                            latitudeDelta: 0.01,
                            longitudeDelta: 0.01
                        }}
                    >
                        <MapView.Marker
                            coordinate={{
                                latitude: -6.117664,
                                longitude: 106.906349
                            }}
                            draggable={false}
                        />
                    </MapView>
                </View>
                <View style={{
                    padding: 20,
                    backgroundColor: colors.lightL
                }}>
                    <DetailLayout
                        title={
                            'Detail Lokasi'
                        }>
                        <Text style={textStyle.content}>
                            {data.detailLoc.detailLoc}
                        </Text>
                    </DetailLayout>
                    <DetailLayout
                        title={
                            'Catatan'
                        }>
                        <Text style={textStyle.content}>
                            {data.note}
                        </Text>
                    </DetailLayout>
                    <DetailLayout
                        title={
                            'Waktu Pemesanan'
                        }>
                        <Text style={textStyle.content}>
                            {moment(data.timeOrder).format('DD-MM-SS HH:mm:ss') + ' WIB'}
                        </Text>
                    </DetailLayout>

                </View>
                <DetailLayout
                    title={
                        'Detail Pesanan'
                    }>
                    <Space size={5} />
                    <Text style={[textStyle.title, {
                        fontSize: 12,
                        color: colors.secondary
                    }]}>
                        {data.title}
                    </Text>
                    <View style={containerStyle.flexBetwen}>
                        <Text style={[textStyle.content, {
                            fontSize: 12,
                            color: colors.grey
                        }]}>
                            {data.nameProduct}
                        </Text>
                        <Text style={[textStyle.title, {
                            color: colors.secondary
                        }]}>
                            {formatNumber(data.priceDetailProduct)}
                        </Text>
                    </View>
                    {
                        data.priceAddOn ? (
                            <View style={containerStyle.flexBetwen}>
                                <Text style={[textStyle.content, {
                                    fontSize: 12,
                                    color: colors.grey
                                }]}>
                                    {data.nameAddOn}
                                </Text>
                                <Text style={[textStyle.title, {
                                    color: colors.secondary
                                }]}>
                                    {formatNumber(data.priceAddOn)}
                                </Text>
                            </View>
                        ) : (
                                null
                            )
                    }
                    <Space size={10} />
                    <View style={{
                        height: 2,
                        width: '100%',
                        backgroundColor: colors.greyL
                    }} />
                    <View style={containerStyle.flexBetwen}>
                        <Text style={[textStyle.content, {
                            fontSize: 12,
                            color: colors.grey
                        }]}>
                            {'Total'}
                        </Text>
                        <Text style={[textStyle.title, {
                            color: colors.secondary
                        }]}>
                            {formatNumber(data.priceAddOn ? data.priceAddOn + data.priceDetailProduct : data.priceDetailProduct)}
                        </Text>
                    </View>
                </DetailLayout>
                <Space size={100} />
            </ScrollView>
            {
                !actions && data.idOrder ? (
                    <View style={[
                        containerStyle.flexCenter,
                        {
                            paddingHorizontal: 20,
                            position: 'absolute',
                            bottom: 10
                        }
                    ]}>
                        <Button
                            color={colors.danger}
                            style={{
                                paddingVertical: 5,
                                borderRadius: 10,
                                flex: 1,
                                marginRight: 10
                            }}
                            mode="contained"
                            onPress={() => {
                                doReject()
                            }}>
                            Tolak
                        </Button>
                        <View style={{
                            paddingRight: 10
                        }}>
                        <CountdownCircleTimer
                                strokeWidth={5}
                                isPlaying
                                duration={70}
                                size={50}
                                onComplete={() => {
                                    doReject()
                                }}
                                colors={[['#004777', 0.33], ['#F7B801', 0.33], ['#A30000']]}
                            >
                                {({ remainingTime, animatedColor }) => (
                                    <Animated.Text style={{ color: animatedColor }}>
                                        {remainingTime}
                                    </Animated.Text>
                                )}
                            </CountdownCircleTimer>
                        </View>
                        <Button
                            color={colors.secondary}
                            style={{
                                paddingVertical: 5,
                                borderRadius: 10,
                                flex: 3,
                                marginRight: 10
                            }}
                            mode="contained"
                            onPress={() => {
                                doAccept(data.idOrder)
                            }}>
                            Terima
                        </Button>
                    </View>
                ) : (
                    null
                )
            }
        </MainHeaderContainer>
    );
};

export default NewOrder;
