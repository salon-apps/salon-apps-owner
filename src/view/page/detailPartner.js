import React, { useState, useEffect } from 'react';
import { useTracked } from '../../service';
import { Space, DetailLayout, MainHeaderContainer } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    Image,
    Alert,
    Linking
} from 'react-native';
import { MainBG } from '../../components/abstrack';
import { FAB, Dialog, Portal, Button } from 'react-native-paper'
import { colors } from '../../styles';
import textStyle from '../../styles/text';
import containerStyle from '../../styles/container';
import moment from 'moment';
import { api } from '../../service/api';
import { LoginButton } from '../../components/button';
import { Input } from '../../components/input';
import { waLinking } from '../../helper/whatsapp';
import { color } from 'react-native-reanimated';


const DetailPartner = ({ navigation, route: {
    params: { data } } }) => {

    const [state, action] = useTracked();
    const [open, setOpen] = useState(false);
    const [openFab, setOpenFab] = useState(false);
    const [saldo, setSaldo] = useState(0);

    useEffect(() => {
        console.log(navigation.dangerouslyGetState())
    }, [])

    async function doSaldo() {
        try {
            setOpen(false)
            action({
                type: 'loadStart'
            })
            const response = await api('addSaldo', {
                idPartner: data.idPartner,
                saldo: Number(data.saldo) + Number(saldo)
            });
            if (response) {
                action({
                    type: 'loadStop'
                })
                if (!response.error) {
                    action({
                        type: 'successAlert',
                        message: `sukses mengirimkan saldo ${saldo} ke ${data.fullName}`
                    })
                }
            }
        } catch (error) {
            action({
                type: 'loadStop'
            })
            action({
                type: 'errorAlert',
                message: error.message
            })
        }
    }

    async function doActivatePartner() {
        try {
            action({
                type: 'loadStart'
            })
            const response = await api('activatePartner', {
                idPartner: data.idPartner,
            });
            if (response) {
                action({
                    type: 'loadStop'
                })
                if (!response.error) {
                    action({
                        type: 'successAlert',
                        message: `sukses mengaktifkan ${data.fullName} sebagai partner baru`
                    })
                    action({
                        type: 'updateState',
                        data: 'updatePartner'
                    })
                    navigation.goBack();
                }
            }
        } catch (error) {
            action({
                type: 'loadStop'
            })
            action({
                type: 'errorAlert',
                message: error.message
            })
        }
    }

    function getFunction() {
        if (data.active === 1) {
            return [
                {
                    icon: 'account-settings',
                    label: 'Ubah Profile',
                    color: colors.primary,
                    onPress: () => {
                        navigation.navigate('Register', {
                            profile: data
                        });
                    },
                },
                {
                    icon: 'coin',
                    label: 'Isi Saldo',
                    color: colors.primary,
                    onPress: () => {
                        setOpen(true)
                    }
                },
                {
                    icon: 'whatsapp',
                    label: `Hubungi ${data.fullName.split(' ')[0]}`,
                    color: colors.primary,
                    onPress: () => {
                        if (data.hp) {
                            waLinking(`Hay ${data.namaLengkap}`, data.hp)
                        } else {
                            Linking.openURL(`mailto:${data.email}?subject=Konfirmasi Kelengkapan Data&body=Hay ${data.fullName} Data Kamu Belum Lengkap Harap Segera Lengkapi Data Mu Agar Dapat Bergabung Dengan Kami`)
                        }
                    }
                }]
        } else {
            if (data.KK && data.NIK) {
                return [{
                    icon: 'account-settings',
                    label: 'Ubah Profile',
                    color: colors.primary,
                    onPress: () => {
                        navigation.navigate('Register', {
                            profile: data
                        });
                    },
                },
                {
                    icon: 'account-check',
                    label: 'Aktifkan Partner',
                    color: colors.primary,
                    onPress: () => {
                        Alert.alert(
                            "Konfirmasi",
                            `Yakin Untuk Mengaktifkan ${data.fullName} ?`,
                            [
                                {
                                    text: "Ask me later",
                                    onPress: () => console.log("Ask me later pressed")
                                },
                                {
                                    text: "Cancel",
                                    onPress: () => console.log("Cancel Pressed"),
                                    style: "cancel"
                                },
                                {
                                    text: "OK", onPress: () => {
                                        doActivatePartner()
                                    }
                                }
                            ],
                            { cancelable: false }
                        );
                    }
                },
                {
                    icon: 'whatsapp',
                    label: `Hubungi ${data.fullName.split(' ')[0]}`,
                    color: colors.primary,
                    onPress: () => {
                        if (data.hp) {
                            waLinking(`Hay ${data.namaLengkap}`, data.hp)
                        } else {
                            Linking.openURL(`mailto:${data.email}?subject=Konfirmasi Kelengkapan Data&body=Hay ${data.fullName} Data Kamu Belum Lengkap Harap Segera Lengkapi Data Mu Agar Dapat Bergabung Dengan Kami`)
                        }
                    }
                }]
            } else {
                return [{
                    icon: 'account-settings',
                    label: 'Ubah Profile',
                    color: colors.primary,
                    onPress: () => {
                        navigation.navigate('Register', {
                            profile: data
                        });
                    },
                },
                {
                    icon: 'whatsapp',
                    label: `Hubungi ${data.fullName.split(' ')[0]}`,
                    color: colors.primary,
                    onPress: () => {
                        if (data.hp) {
                            waLinking(`Hay ${data.namaLengkap}`, data.hp)
                        } else {
                            Linking.openURL(`mailto:${data.email}?subject=Konfirmasi Kelengkapan Data&body=Hay ${data.fullName} Data Kamu Belum Lengkap Harap Segera Lengkapi Data Mu Agar Dapat Bergabung Dengan Kami`)
                        }
                    }
                }]
            }
        }
    }

    return (
        <MainHeaderContainer
            title={`Partner #${data.ID} ${data.fullName.split(' ')[0]}`}
            dark={false}
            back
            barColor={colors.primaryD}
            nav={navigation}>
            <MainBG />
            <Portal>
                <Dialog visible={open} onDismiss={() => {
                    setOpen(false)
                }}>
                    <Dialog.Title>Kirim Saldo</Dialog.Title>
                    <Dialog.Content>
                        <Input
                            title={'jumlah saldo'}
                            value={saldo}
                            keyboardType={'numeric'}
                            action={text => {
                                setSaldo(text)
                            }}
                        />
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button onPress={() => {
                            doSaldo()
                        }}>Kirim Saldo</Button>
                    </Dialog.Actions>
                </Dialog>
            </Portal>
            <Portal>
                <FAB.Group
                    color={colors.light}
                    fabStyle={{
                        backgroundColor: colors.primary,
                    }}
                    open={openFab}
                    icon={openFab ? 'close' : 'settings'}
                    actions={
                        getFunction()
                    }
                    onStateChange={() => {
                        setOpenFab(!openFab)
                    }}
                    onPress={() => {
                        setOpenFab(!openFab)
                    }}
                />
            </Portal>
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <Space size={50} />
                <TouchableOpacity style={{
                    alignSelf: 'center'
                }}>
                    <Image source={
                        data.partnerImage ? {
                            uri: data.partnerImage
                        } :
                            require('../../../assets/images/default-profile.png')
                    } style={{
                        height: 150,
                        width: 150,
                        resizeMode: 'cover',
                        borderRadius: 100,
                        borderWidth: 2,
                        borderColor: colors.light
                    }} />
                </TouchableOpacity>
                <View style={{
                    padding: 20
                }}>
                    <Text style={[textStyle.title, {
                        fontSize: 24,
                        color: colors.dark
                    }]}>{data.fullName}</Text>
                    <Text style={[textStyle.title, {
                        fontSize: 20,
                        lineHeight: 30,
                        color: colors.dark
                    }]}>{data.email}</Text>
                </View>
                <DetailLayout
                    title={
                        'Tanggal Bergabung'
                    }>
                    <Text style={[textStyle.subTitle]}>{moment(data.dateJoin).format('DD/MM/YYYY')}</Text>
                </DetailLayout>
                <DetailLayout
                    title={
                        'Nama Lengkap'
                    }>
                    <Text style={[textStyle.subTitle]}>{data.namaLengkap}</Text>
                </DetailLayout>
                <DetailLayout
                    title={
                        'Alamat Lengkap'
                    }>
                    <Text style={[textStyle.subTitle]}>{data.alamatLengkap}</Text>
                </DetailLayout>
                <DetailLayout
                    title={
                        'Kontak Darurat'
                    }>
                    <Text style={[textStyle.subTitle]}>{`Nama: ${data.namaDarurat} (${data.hubDarurat})`}</Text>
                    <Text style={[textStyle.subTitle]}>{`Alamat: ${data.alamatDarurat}`}</Text>
                    <Text style={[textStyle.subTitle]}>{`Hp: ${data.hpDarurat}`}</Text>
                </DetailLayout>
                <DetailLayout
                    title={
                        'Nomor KK'
                    }>
                    <Text style={[textStyle.subTitle]}>{data.KK}</Text>
                </DetailLayout>
                <DetailLayout
                    title={
                        'Nomor KTP'
                    }>
                    <Text style={[textStyle.subTitle]}>{data.NIK}</Text>
                </DetailLayout>
                <DetailLayout
                    title={
                        'Nomor HP'
                    }>
                    <Text style={[textStyle.subTitle]}>{data.hp}</Text>
                </DetailLayout>
                <DetailLayout
                    title={
                        'Status Keanggotaan'
                    }>
                    <View style={{
                        marginVertical: 30,
                        marginHorizontal: 50,
                        borderWidth: 2,
                        borderColor: colors.primary,
                        borderStyle: 'dashed',
                        borderRadius: 1
                    }}>
                        <Text style={[textStyle.title, {
                            color: colors.primary,
                            padding: 20,
                            textAlign: 'center'
                        }]}>{
                                data.active === 1 ? 'aktif' : data.KK && data.NIK ? 'belum aktif' : 'data belum lengkap'
                            }</Text>
                    </View>
                </DetailLayout>
                <Space size={50} />
            </ScrollView>
            <View style={{
                position: 'absolute',
                bottom: 0,
                width: '100%',
                padding: 20,
            }}>
                <View style={containerStyle.flexCenter}>
                    {
                        data.active === 1 ? (
                            <>
                                {/* <Button
                                    color={colors.primary}
                                    style={{
                                        paddingVertical: 5,
                                        borderRadius: 10,
                                        flex: 1,
                                        marginRight: 10
                                    }}
                                    mode="contained"
                                    onPress={() => {
                                        setOpen(true)
                                    }}>
                                    Isi Saldo
                                </Button>
                                <Button
                                    color={colors.whatsapp}
                                    style={{
                                        paddingVertical: 5,
                                        borderRadius: 10,
                                        flex: 5
                                    }}
                                    mode="contained"
                                    onPress={() => {
                                        if (data.hp) {
                                            waLinking(`Hay ${data.namaLengkap}`, data.hp)
                                        } else {
                                            Linking.openURL(`mailto:${data.email}?subject=Konfirmasi Kelengkapan Data&body=Hay ${data.fullName} Data Kamu Belum Lengkap Harap Segera Lengkapi Data Mu Agar Dapat Bergabung Dengan Kami`)
                                        }
                                    }}>
                                    {`Hubungi ${data.fullName.split(' ')[0]}`}
                                </Button> */}
                            </>
                        ) : (
                                data.KK && data.NIK ? (
                                    <>
                                        <Button
                                            color={colors.primary}
                                            style={{
                                                paddingVertical: 5,
                                                borderRadius: 10,
                                                flex: 1,
                                                marginRight: 10
                                            }}
                                            mode="contained"
                                            onPress={() => {
                                                Alert.alert(
                                                    "Konfirmasi",
                                                    `Yakin Untuk Mengaktifkan ${data.fullName} ?`,
                                                    [
                                                        {
                                                            text: "Ask me later",
                                                            onPress: () => console.log("Ask me later pressed")
                                                        },
                                                        {
                                                            text: "Cancel",
                                                            onPress: () => console.log("Cancel Pressed"),
                                                            style: "cancel"
                                                        },
                                                        {
                                                            text: "OK", onPress: () => {
                                                                doActivatePartner()
                                                            }
                                                        }
                                                    ],
                                                    { cancelable: false }
                                                );
                                            }}>
                                            Partner Ini Belum Active
                                        </Button>
                                        {/* <Button
                                            color={colors.whatsapp}
                                            style={{
                                                paddingVertical: 5,
                                                borderRadius: 10,
                                                flex: 5
                                            }}
                                            mode="contained"
                                            onPress={() => {
                                                if (data.hp) {
                                                    waLinking(`Hay ${data.namaLengkap}`, data.hp)
                                                } else {
                                                    Linking.openURL(`mailto:${data.email}?subject=Konfirmasi Kelengkapan Data&body=Hay ${data.fullName} Data Kamu Belum Lengkap Harap Segera Lengkapi Data Mu Agar Dapat Bergabung Dengan Kami`)
                                                }
                                            }}>
                                            {`Hubungi ${data.fullName.split(' ')[0]}`}
                                        </Button> */}
                                    </>
                                ) : (
                                        <>
                                            {/* <Button
                                                color={colors.whatsapp}
                                                style={{
                                                    paddingVertical: 5,
                                                    borderRadius: 10,
                                                    flex: 5
                                                }}
                                                mode="contained"
                                                onPress={() => {
                                                    if (data.hp) {
                                                        waLinking(`Hay ${data.namaLengkap}`, data.hp)
                                                    } else {
                                                        Linking.openURL(`mailto:${data.email}?subject=Konfirmasi Kelengkapan Data&body=Hay ${data.fullName} Data Kamu Belum Lengkap Harap Segera Lengkapi Data Mu Agar Dapat Bergabung Dengan Kami`)
                                                    }
                                                }}>
                                                {`Hubungi ${data.fullName.split(' ')[0]}`}
                                            </Button> */}
                                        </>
                                    )
                            )
                    }
                </View>
            </View>
        </MainHeaderContainer>
    );
};

export default DetailPartner;
