import React, { useEffect } from 'react';
import { useTracked } from '../../service';
import { MainHeaderContainer } from '../../components/containers';
import {
    ScrollView,
    View,
    Image
} from 'react-native';
import { MainBG } from '../../components/abstrack';
import { colors } from '../../styles';
import containerStyle from '../../styles/container';
import { ListArticle } from '../../components/card';
import image, { imageCategory, imageSubCategory } from '../../constant/image';


const Category = ({ route, navigation }) => {

    const [state, dispatch] = useTracked();

    const { title, data } = route.params;

    // useEffect(() => {
    //     console.log('**********')
    //     console.log(data[0].idProduct)
    // }, []);

    function goDetails(data) {        
        navigation.navigate('Details', {
            title: data.nameProduct,
            payload: data,
            ID: data.idProduct
        });
    }

    return (
        <MainHeaderContainer
            title={title}
            dark={false}
            back
            barColor={colors.primaryD}
            nav={navigation}>
            <MainBG />
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <View style={[
                    containerStyle.list,
                    {
                        marginTop: 20
                    }
                ]}>
                    {data.map((row, i) => (
                        <View key={i}>
                            <ListArticle
                                onPress={() => {
                                    goDetails(row)
                                }}
                                title={row.nameProduct}
                                content={row.descProduct}
                                icon={imageSubCategory(row.idProduct)}
                                update={() => {
                                    goDetails(row)
                                }}
                            />
                        </View>
                    ))}
                </View>
            </ScrollView>
        </MainHeaderContainer>
    );
};

export default Category;
