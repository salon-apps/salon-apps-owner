import React, { useEffect, useState } from 'react';
import messaging from '@react-native-firebase/messaging';
import { getUniqueId } from 'react-native-device-info';
import { useTracked } from '../../service';
import { MainContainer, Space } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    Image,
    StatusBar
} from 'react-native';
import { MainBG } from '../../components/abstrack';
import AsyncStorage from '@react-native-community/async-storage';
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import { api } from '../../service/api';
import { colors } from '../../styles';
import textStyle from '../../styles/text';
import { getProfile, jwtVerify, encriptor } from '../../helper/enct';
import {
    LoginButton, GFLogin,
} from '../../components/button';
import { str } from '../../constant/string';
import containerStyle from '../../styles/container';
import { Input } from '../../components/input';
import { emailValidator } from '../../helper/validator';

const Login = ({ navigation }) => {
    const [state, action] = useTracked();
    const [render, setRender] = useState(false);

    const [email, setEmail] = useState('');
    const [pass, setPass] = useState('');
    // const [emailKeybord, setEmailKeybord] = useState('email-address');

    async function updateDeviceID(data, unregistered) {
        try {
            const request = await api('updateDevice', data)
            if (!request.error) {
                const userProfile = await getProfile();
                setRender(true)
                console.log(userProfile)
                if (userProfile.idPartner) {
                    action({
                        type: 'updateProfile',
                        data: userProfile
                    })
                    if (userProfile.role === 'owner') {
                        navigation.reset({
                            index: 0,
                            routes: [{ name: 'Tabs' }],
                        });
                    } else {
                        if (unregistered) {
                            action({ type: 'errorAlert', message: 'Satu langkah lagi untuk menjadi mitra kami' })
                            navigation.navigate('Register');
                        } else {
                            navigation.reset({
                                index: 0,
                                routes: [{ name: 'TabsPartner' }],
                            });
                        }
                    }
                }
            } else {
                console.log(request)
                action({ type: 'errorAlert', message: request.message })
                setRender(true)
            }
        } catch (error) {
            console.log(error)
            alert(error.message)
        }
    }

    function getTokenFCM(ID, unregistered) {
        // Get the device token        
        messaging()
            .getToken()
            .then(token => {
                console.log(token)
                updateDeviceID({
                    deviceID: ID,
                    firebaseToken: token
                }, unregistered)
            });

        // Listen to whether the token changes
        return messaging().onTokenRefresh(token => {
            alert('token refresh')
            updateDeviceID({
                deviceID: ID,
                firebaseToken: token
            }, unregistered)
        });
    }

    function getDeviceID(unregistered) {
        const ID = getUniqueId()
        if (ID) {
            getTokenFCM(ID, unregistered);
        } else {
            // setRender(true)
            alert('Device Tidak Didukung')
        }
    }

    async function auth() {
        const user = await AsyncStorage.getItem('token')
        const unregistered = await AsyncStorage.getItem('unregistered');        
        console.log(user)
        if (user !== null) {
            if(unregistered){
                if(unregistered === 'true'){
                    getDeviceID(true)
                } else {
                    getDeviceID()
                }
            } else {
                getDeviceID()
            }
        } else {
            setRender(true)
        }
    }

    useEffect(() => {
        auth()
    }, [])

    async function googleReset() {
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
        } catch (error) {
            console.error(error);
        }
    };

    async function googleSignIn() {
        try {
            action({ type: 'loadStart' })
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            const data = await api('loginGoogle', {
                tokenID: userInfo.idToken
            })
            loginProsess(data, true)
        } catch (error) {
            action({ type: 'loadStop' })
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                console.log('user cancelled the login flow')
            } else if (error.code === statusCodes.IN_PROGRESS) {
                console.log('operation (e.g. sign in) is in progress already')
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                alert('play services not available or outdated')
            } else {
                action({ type: 'errorAlert', message: error.message })
                console.log(error)
            }
        }
    };

    async function loginProsess(data, google) {

        async function storeToken() {
            const token = data.data
            await AsyncStorage.setItem('token', token)
            const userProfile = await jwtVerify(token);
            if (userProfile.payload.idPartner) {
                console.log('aaaaaaaaaaa', data.errorCode)
                if (data.errorCode === 'unregistered') {
                    await AsyncStorage.setItem('unregistered', 'true')
                    getDeviceID(true)
                } else {
                    getDeviceID()
                }
            }
            if (google) {
                await googleReset()
            }
            action({ type: 'loadStop' })
        }

        // if (data.error) {
        //     action({ type: 'loadStop' });
        //     action({ type: 'errorAlert', message: data.message })
        // } else {
        storeToken()
        // }
    }

    async function emailLogin() {
        try {
            action({ type: 'loadStart' })
            const data = await api('login', {
                username: email,
                pass: pass.length < 1 ? 'null' : encriptor(pass)
            })
            loginProsess(data)
        } catch (error) {
            action({ type: 'loadStop' })
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }


    //// "errorCode": "unregistered",
    function doLogin() {
        if (email.toString().substring(0, 2) === '08' || email.toString().substring(0, 3) === '628') {
            emailLogin()
        } else {
            if (!emailValidator(email)) {
                action({ type: 'errorAlert', message: 'Format Email Salah' })
            } else {
                emailLogin()
            }
        }
    }

    if (render) {
        return (
            <MainContainer dark={false} barColor={colors.primaryD}>
                <MainBG />
                <ScrollView
                    keyboardShouldPersistTaps={'handled'}
                    contentContainerStyle={{
                        flexGrow: 1,
                        justifyContent: 'space-between',
                        flexDirection: 'column',
                        background: 'transparent'
                    }}
                >
                    <View style={{
                        padding: 20,
                        marginTop: 20
                    }}>
                        <Text style={textStyle.subWelcomeLight}>{'selamat datang di'}</Text>
                        <Text style={textStyle.welcomeLight}>{'jelita salon & spa homeservices'}</Text>
                    </View>
                    <View style={{
                        marginBottom: 10
                    }}>
                        <View style={containerStyle.card}>
                            <Input
                                title={'email'}
                                value={email}
                                type={'email'}
                                action={text => {
                                    setEmail(text)
                                }}
                            />
                            <Input
                                title={'password'}
                                type={'password'}
                                value={pass}
                                action={text => {
                                    setPass(text)
                                }}
                            />
                            <Space size={20} />
                            <LoginButton lable={str.signIn} onPress={() => {
                                doLogin()
                            }} />
                            <Space size={10} />
                            <Text style={[
                                textStyle.subTitle,
                                {
                                    textAlign: 'center'
                                }
                            ]}>{'Login / Register'}</Text>
                            <Space size={10} />
                            <View>
                                <GFLogin
                                    GAction={() => {
                                        googleSignIn()
                                    }} FAction={() => {
                                        action({ type: 'errorAlert', message: 'comming soon' })
                                    }} />
                            </View>
                        </View>
                        <Image
                            style={{
                                margin: 40,
                                width: 100,
                                height: 100,
                                resizeMode: 'contain',
                                alignSelf: 'center'
                            }} source={require('../../../assets/images/icon.png')} />
                    </View>
                </ScrollView>
            </MainContainer>
        )
    } else {
        return (
            <View style={[
                {
                    height: '100%',
                    width: '100%',
                    backgroundColor: colors.primary
                },
                containerStyle.flexCenter
            ]}>
                <StatusBar translucent backgroundColor="transparent" />
                <View>
                    <Image
                        style={{
                            margin: 10,
                            width: 150,
                            height: 150,
                            resizeMode: 'contain',
                            alignSelf: 'center'
                        }}
                        source={require('../../../assets/loading/gear.gif')} />
                    <Text style={textStyle.subWelcomeLight}>sedang mengambil data</Text>
                </View>
            </View>
        )
    }
};

export default Login;
