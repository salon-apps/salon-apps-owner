import React, { useEffect, useState } from 'react';
import messaging from '@react-native-firebase/messaging';
import { getUniqueId } from 'react-native-device-info';
import { useTracked } from '../../service';
import {
    View,
    SafeAreaView,
    Text,
    Image,
    StyleSheet,
    StatusBar,
    TouchableOpacity,
} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import AsyncStorage from '@react-native-community/async-storage';
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import { api } from '../../service/api';
import { colors } from '../../styles';
import { getProfile, jwtVerify, decriptor } from '../../helper/enct';
import {
    GFLogin,
} from '../../components/button';
import images from '../../constant/image';
import containerStyle from '../../styles/container';
import textStyle from '../../styles/text';
import { Space } from '../../components/containers';
import { key } from '../../constant/key';

const Intro = ({ navigation }) => {
    const [state, action] = useTracked();
    const [render, setRender] = useState(false);

    useEffect(() => {
        auth()
    }, [])

    async function updateDeviceID(data, unregistered) {
        try {
            const request = await api('updateDevice', data)
            if (!request.error) {
                const userProfile = await getProfile();
                setRender(true)
                console.log(userProfile)
                if (userProfile.idPartner) {
                    action({
                        type: 'updateProfile',
                        data: userProfile
                    })
                    if (userProfile.role === 'owner') {
                        navigation.reset({
                            index: 0,
                            routes: [{ name: 'Tabs' }],
                        });
                    } else {
                        if (unregistered) {
                            action({ type: 'errorAlert', message: 'Satu langkah lagi untuk menjadi mitra kami' })
                            navigation.navigate('Register');
                        } else {
                            navigation.reset({
                                index: 0,
                                routes: [{ name: 'TabsPartner' }],
                            });
                        }
                    }
                }
            } else {
                console.log(request)
                action({ type: 'errorAlert', message: request.message })
                setRender(true)
            }
        } catch (error) {
            console.log(error)
            alert(error.message)
        }
    }

    function getTokenFCM(ID, unregistered) {
        // Get the device token        
        messaging()
            .getToken()
            .then(token => {
                console.log(token)
                updateDeviceID({
                    deviceID: ID,
                    firebaseToken: token
                }, unregistered)
            });

        // Listen to whether the token changes
        return messaging().onTokenRefresh(token => {
            alert('token refresh')
            updateDeviceID({
                deviceID: ID,
                firebaseToken: token
            }, unregistered)
        });
    }

    function getDeviceID(unregistered) {
        const ID = getUniqueId()
        if (ID) {
            getTokenFCM(ID, unregistered);
        } else {
            // setRender(true)
            alert('Device Tidak Didukung')
        }
    }

    async function auth() {
        const user = await AsyncStorage.getItem('token')
        const unregistered = await AsyncStorage.getItem('unregistered');
        if (user !== null) {
            if (unregistered) {
                if (unregistered === 'true') {
                    getDeviceID(true)
                } else {
                    getDeviceID()
                }
            } else {
                getDeviceID()
            }
        } else {
            setRender(true)
            checkOwner()
        }
    }

    async function checkOwner() {
        try {
            const ID = getUniqueId();
            let ownerID = ''            
            const payload = await api('ownerInfo')
            if(!payload.error){
                ownerID = payload.data.deviceID
            }
            if (ID === ownerID) {
                action({ type: 'loadStart' })
                const data = await api('login', {
                    username: decriptor(key.ownerEmail),
                    pass: key.ownerPass
                })
                // console.log(data)
                loginProsess(data)
            }
        } catch (error) {
            action({ type: 'loadStop' })
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    async function googleReset() {
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
        } catch (error) {
            console.error(error);
        }
    };

    async function loginProsess(data, google) {

        async function storeToken() {
            const token = data.data
            await AsyncStorage.setItem('token', token)
            const userProfile = await jwtVerify(token);
            if (userProfile.payload.idPartner) {
                if (data.errorCode === 'unregistered') {
                    await AsyncStorage.setItem('unregistered', 'true')
                    getDeviceID(true)
                } else {
                    getDeviceID()
                }
            }
            if (google) {
                await googleReset()
            }
            action({ type: 'loadStop' })
        }
        storeToken()
    }

    async function googleSignIn() {
        try {
            action({ type: 'loadStart' })
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            const data = await api('loginGoogle', {
                tokenID: userInfo.idToken
            })
            loginProsess(data, true)
        } catch (error) {
            action({ type: 'loadStop' })
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                console.log('user cancelled the login flow')
            } else if (error.code === statusCodes.IN_PROGRESS) {
                console.log('operation (e.g. sign in) is in progress already')
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                alert('play services not available or outdated')
            } else {
                action({ type: 'errorAlert', message: error.message })
                console.log(error)
            }
        }
    };


    // start slider

    const data = [
        {
            title: 'Hair Treatment',
            text: 'Perawatan rambut kapan pun dimanapun',
            image: images.slide2,
            bg: colors.primaryD,
        },
        {
            title: 'Nail Treatment',
            text: 'Perawatan kuku kapanpun dimanapun',
            image: images.slide1,
            bg: colors.warningD,
        },
        {
            title: 'Message',
            text: "Pijat refleksi sehat siap jemput kapan pun dimanapun",
            image: images.slide5,
            bg: colors.secondary,
        },
    ];

    const styles = StyleSheet.create({
        slide: {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'blue',
        },
        image: {
            height: 180,
            width: 350,
            marginVertical: 32
        },
        text: {
            color: 'rgba(255, 255, 255, 0.8)',
            textAlign: 'center',
        },
        title: {
            fontSize: 22,
            color: 'white',
            textAlign: 'center',
        },
        paginationContainer: {
            position: 'absolute',
            bottom: 16,
            left: 16,
            right: 16,
        },
        paginationDots: {
            height: 16,
            margin: 16,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        },
        dot: {
            width: 10,
            height: 10,
            borderRadius: 5,
            marginHorizontal: 4,
        },
        buttonContainer: {
            flexDirection: 'row',
            marginHorizontal: 24,
        },
        button: {
            flex: 1,
            paddingVertical: 15,
            marginHorizontal: 8,
            borderRadius: 24,
            backgroundColor: '#1cb278',
        },
        buttonText: {
            color: 'white',
            fontWeight: '600',
            textAlign: 'center',
        },
    });


    const _renderItem = ({ item }) => {
        return (
            <View
                style={[
                    styles.slide,
                    {
                        backgroundColor: item.bg,
                    },
                ]}>
                <Text style={styles.title}>{item.title}</Text>
                <Image source={item.image} style={styles.image} />
                <Text style={styles.text}>{item.text}</Text>
            </View>
        );
    };

    const _keyExtractor = (item) => item.title;

    const _renderPagination = (activeIndex) => {
        return (
            <View style={styles.paginationContainer}>
                <SafeAreaView>
                    <View style={styles.paginationDots}>
                        {data.length > 1 &&
                            data.map((_, i) => (
                                <TouchableOpacity
                                    key={i}
                                    style={[
                                        styles.dot,
                                        i === activeIndex
                                            ? { backgroundColor: 'white' }
                                            : { backgroundColor: 'rgba(0, 0, 0, .2)' },
                                    ]}
                                    onPress={() => this.goToSlide(i, true)}
                                />
                            ))}
                    </View>
                    <Text style={[textStyle.subWelcomeLight,
                    {
                        textAlign: 'center',
                        margin: 5
                    }]}>login / register</Text>
                    <View style={styles.buttonContainer}>
                        <GFLogin
                            GAction={() => {
                                googleSignIn()
                            }} FAction={() => {
                                action({ type: 'errorAlert', message: 'comming soon' })
                            }} />
                    </View>
                </SafeAreaView>
            </View>
        );
    };

    // end slider

    if (render) {
        return (
            <View style={{ flex: 1 }}>
                <StatusBar translucent backgroundColor="transparent" />
                <AppIntroSlider
                    keyExtractor={_keyExtractor}
                    renderItem={_renderItem}
                    renderPagination={_renderPagination}
                    data={data}
                />
            </View>
        )
    } else {
        return (
            <View style={[
                {
                    height: '100%',
                    width: '100%',
                    backgroundColor: colors.primary
                },
                containerStyle.flexCenter
            ]}>
                <StatusBar translucent backgroundColor="transparent" />
                <View>
                    <Image
                        style={{
                            margin: 10,
                            width: 150,
                            height: 150,
                            resizeMode: 'contain',
                            alignSelf: 'center'
                        }}
                        source={require('../../../assets/loading/gear.gif')} />
                    <Text style={textStyle.subWelcomeLight}>sedang mengambil data</Text>
                </View>
            </View>
        )
    }
};

export default Intro;
