import React, { useState, useEffect } from 'react';
import ImagePicker from 'react-native-image-picker';
import { MainContainer } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    Image,
    Platform
} from 'react-native';
import { SecondaryBG } from '../../components/abstrack';
import {
    str
} from '../../constant/string';
import { Button } from 'react-native-paper';
import { colors } from '../../styles';
import containerStyle from '../../styles/container';
import textStyle from '../../styles/text';
import { Input } from '../../components/input';
import { LoginButton, TNCButton } from '../../components/button';
import { api } from '../../service/api';
import { useTracked } from '../../service';

const Login = ({ navigation, route: {
    params } }) => {
    const [state, action] = useTracked();
    const [name, setName] = useState('');
    const [alamat, setAlamat] = useState('');
    const [darurat, setDarurat] = useState('contoh Ningsih');
    const [hubDar, setHubDar] = useState('contoh IBU');
    const [alamatDar, setAlamatDar] = useState('');
    const [hpDar, setHpDar] = useState('');
    const [nik, setNik] = useState('');
    const [kk, setKk] = useState('');
    const [hp, setHp] = useState('');

    const [check, setCheck] = useState(false);
    const [ID, setID] = useState(false);
    const [image, setImage] = useState(false)

    useEffect(() => {
        getProfile()
    }, [])

    function getProfile() {
        if (params) {
            console.log(params.profile)
            if (params.profile) {
                let data = params.profile
                if (data.partnerImage) {
                    setImage(data.partnerImage)
                }
                if (data.idDetailPartner) {
                    setCheck(true)
                    setID(data.idDetailPartner)
                }
                if (data.namaLengkap) {
                    setName(data.namaLengkap)
                }
                if (data.alamatLengkap) {
                    setAlamat(data.alamatLengkap)
                }
                if (data.namaDarurat) {
                    setDarurat(data.namaDarurat)
                }
                if (data.hubDarurat) {
                    setHubDar(data.hubDarurat)
                }
                if (data.hpDarurat) {
                    setHpDar(data.hpDarurat)
                }
                if (data.alamatDarurat) {
                    setAlamatDar(data.alamatDarurat)
                }
                if (data.NIK) {
                    setNik(data.NIK)
                }
                if (data.KK) {
                    setKk(data.KK)
                }
                if (data.hp) {
                    setHp(data.hp)
                }
            }
        }
    }

    async function doUpdate() {
        try {
            action({ type: 'loadStart' })
            const data = await api('updateDetailPartner', {
                idDetailPartner: ID,
                name,
                alamat,
                darurat,
                hubDar,
                alamatDar,
                hpDar,
                nik,
                kk,
                hp
            })
            if (data) {
                action({ type: 'loadStop' })
                if (!data.error) {
                    action({ type: 'successAlert' })                   
                    action({
                        type: 'updateState',
                        data: 'updatePartner'
                    })
                }
            }
        } catch (error) {
            action({ type: 'loadStop' })
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    async function doRegister() {
        try {
            action({ type: 'loadStart' })
            const data = await api('detailRegister', {
                name,
                alamat,
                darurat,
                hubDar,
                alamatDar,
                hpDar,
                nik,
                kk,
                hp
            })
            if (data) {
                action({ type: 'loadStop' })
                if (!data.error) {
                    action({ type: 'successAlert' })
                    navigation.reset({
                        index: 0,
                        routes: [{ name: 'TabsPartner' }],
                    });
                }
            }
        } catch (error) {
            action({ type: 'loadStop' })
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    async function doUpdateImage(photo) {
        let data = params.profile;
        let payload = new FormData();
        payload.append("file", {
            name: photo.fileName,
            type: photo.type,
            uri:
                Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
        });
        const response = await api('updateImagePartner', {
            payload,
            idPartner: data.idPartner
        })
        if (!response.error) {
            action({ type: 'successAlert', message: 'Sukses Update Partner Image' })            
        }
    }

    function openImagePicker() {
        const options = {
            title: 'Select Avatar',
            customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.launchImageLibrary(options, (response) => {            

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                // const source = { uri: response.uri };

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                setImage(response.uri)
                doUpdateImage(response)
            }
        });
    }

    return (
        <MainContainer dark={false} barColor={colors.secondary}>
            <SecondaryBG />
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'space-between',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <View style={{
                    padding: 20
                }}>
                    <Text style={textStyle.welcomeLight}>{ID ? 'Update Profile' : str.register}</Text>
                    <Text style={textStyle.subWelcomeLight}>{str.account}</Text>
                </View>
                {
                    ID ? (
                        <>
                            <View style={{
                                alignSelf: 'center',
                                marginVertical: 20
                            }}>
                                <Image source={
                                    image ? {
                                        uri: image
                                    } :
                                        require('../../../assets/images/default-profile.png')
                                } style={{
                                    height: 200,
                                    width: 200,
                                    resizeMode: 'cover',
                                    borderRadius: 100,
                                    borderWidth: 2,
                                    borderColor: colors.light
                                }} />
                            </View>
                            <View style={{
                                paddingHorizontal: 10,
                                marginBottom: 10,
                                alignItems: 'center'
                            }}>
                                <Button
                                    color={colors.primary}
                                    style={{
                                        width: 200
                                    }}
                                    mode="contained"
                                    onPress={() => {
                                        openImagePicker()
                                    }}>
                                    Ubah Foto
                            </Button>
                            </View>
                        </>
                    ) : (null)
                }
                <View style={containerStyle.card}>
                    <Input
                        title={'Nama Lengkap'}
                        value={name}
                        action={text => {
                            setName(text)
                        }}
                    />
                    <Input
                        title={'Alamat Lengkap'}
                        value={alamat}
                        action={text => {
                            setAlamat(text)
                        }}
                    />
                    <Input
                        title={'Kontak Darurat'}
                        value={darurat}
                        action={text => {
                            setDarurat(text)
                        }}
                    />
                    <Input
                        title={'Hubungan Kontak Darurat'}
                        value={hubDar}
                        action={text => {
                            setHubDar(text)
                        }}
                    />
                    <Input
                        title={'HP Kontak Darurat'}
                        value={hpDar}
                        action={text => {
                            setHpDar(text)
                        }}
                    />
                    <Input
                        title={'Alamat Kontak Darurat'}
                        value={alamatDar}
                        action={text => {
                            setAlamatDar(text)
                        }}
                    />
                    <Input
                        title={'Nomor NIK'}
                        value={nik}
                        action={text => {
                            setNik(text)
                        }}
                    />
                    <Input
                        title={'Nomor Kartu Keluarga'}
                        value={kk}
                        action={text => {
                            setKk(text)
                        }}
                    />
                    <Input
                        title={'Nomor HP'}
                        value={hp}
                        action={text => {
                            setHp(text)
                        }}
                    />
                </View>
                <View>
                    {
                        ID ? (null) : (
                            <TNCButton check={check} onCheck={() => {
                                setCheck(!check)
                            }} />
                        )
                    }
                    <LoginButton lable={ID ? 'Update Profile' : str.register} onPress={() => {
                        if (check) {
                            if(ID){
                                doUpdate()
                            } else {
                                doRegister()
                            }
                        } else {
                            alert('Syarat dan Ketentuan Belum Disetujui')
                        }
                    }} />
                </View>
            </ScrollView>
        </MainContainer>
    );
};

export default Login;
