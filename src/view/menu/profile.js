import React, { useState, useEffect } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { Button, Switch } from 'react-native-paper';
import { useTracked } from '../../service';
import { MainContainer, Space, TabsContainer, DetailLayout } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    Image,
    RefreshControl
} from 'react-native';
import { LoginButton } from '../../components/button';
import { SecondaryBG } from '../../components/abstrack';
import { colors } from '../../styles';
import textStyle from '../../styles/text';
import { formatNumber } from '../../helper/validator';
import { api } from '../../service/api';
import MapView from 'react-native-maps';
import containerStyle from '../../styles/container';
import GetLocation from 'react-native-get-location';
import getDirections from 'react-native-google-maps-directions';
import { RatingComponent } from '../../components/card';

const initProfile = {
    "active": 1,
    "email": "",
    "fingerPrint": null,
    "fullName": "",
    "hp": null,
    "idPartner": false,
    "latitude": null,
    "longitude": null,
    "partnerImage": false,
    "password": null,
    "ready": 0,
    "role": "partner",
    "saldo": 0,
    "totalOrder": 0,
    "ratingCount": 0
}

const Profile = ({ navigation }) => {

    const [state, action] = useTracked();
    const [refreshing, setRefreshing] = useState(false);
    const [region, setRegion] = useState({
        latitude: -6.117664,
        longitude: 106.906349,
        latitudeDelta: 1,
        longitudeDelta: 1
    })
    const [profile, setProfile] = useState(initProfile);

    useEffect(() => {
        getProfile()
    }, []);

    function onRefresh() {
        setRefreshing(true)
        getProfile()
        setTimeout(() => {
            setRefreshing(false)
        }, 3000);
    }

    async function getProfile() {
        try {
            action({ type: 'loadStart' })
            const request = await api('detailPartner')

            if (request) {
                action({ type: 'loadStop' })
                if (request.error) {
                    action({ type: 'errorAlert', message: request.message })
                } else {
                    setProfile(request.data[0])
                }
            }
        } catch (error) {
            action({ type: 'errorAlert', message: error.message })
            action({ type: 'loadStop' })
        }
    }

    async function updateLoc(params) {
        try {
            action({ type: 'loadStart' })
            const request = await api('updateLoc', params)

            if (request) {
                action({ type: 'loadStop' })
                if (request.error) {
                    action({ type: 'errorAlert', message: request.message })
                } else {
                    action({ type: 'successAlert', message: 'Sukses Update Lokasi' })
                }
            }
        } catch (error) {
            action({ type: 'errorAlert', message: error.message })
            action({ type: 'loadStop' })
        }
    }

    async function updateReady(params) {
        try {
            action({ type: 'loadStart' })
            const request = await api('updateReady', params)

            if (request) {
                action({ type: 'loadStop' })
                if (request.error) {
                    action({ type: 'errorAlert', message: request.message })
                } else {
                    getProfile()
                }
            }
        } catch (error) {
            action({ type: 'errorAlert', message: error.message })
            action({ type: 'loadStop' })
        }
    }

    function getMyLocation() {

        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 15000,
        })
            .then(location => {
                const newRegion = {
                    latitude: location.latitude,
                    longitude: location.longitude,
                    latitudeDelta: 0.001,
                    longitudeDelta: 0.001
                };
                updateLoc({
                    latitude: location.latitude,
                    longitude: location.longitude
                })
                setRegion(newRegion)
                action({ type: 'loadStop' });
            })
            .catch(error => {
                const { code, message } = error;
                console.warn(code, message);
            })
    }

    async function logOut() {
        await AsyncStorage.removeItem('token')
        navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
        });
    }

    return (
        <TabsContainer>
            <SecondaryBG />
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={() => {
                            onRefresh()
                        }}
                    />
                }
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <View style={[
                    {
                        padding: 20
                    },
                    containerStyle.flexBetwen
                ]}>
                    <Text style={[textStyle.title, {
                        fontSize: 24,
                        color: colors.light
                    }]}>{'Profile Ku'}</Text>
                    <Switch style={{
                        marginTop: 5
                    }}
                        value={profile.ready === 0 ? false : true}
                        color={colors.primary}
                        onValueChange={() => {
                            updateReady({
                                ready: profile.ready === 0 ? 1 : 0
                            })
                        }} />
                </View>
                <Space size={30} />
                <View style={{
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                    <TouchableOpacity>
                        <Image source={
                            profile.partnerImage ? {
                                uri: profile.partnerImage
                            } :
                                require('../../../assets/images/default-profile.png')
                        } style={{
                            height: 100,
                            width: 100,
                            resizeMode: 'cover',
                            borderRadius: 100,
                            borderWidth: 2,
                            borderColor: colors.light
                        }} />
                    </TouchableOpacity>
                    <Space size={15} />
                    <RatingComponent value={profile.ratingCount} />
                    <Space size={15} />
                    <Text style={textStyle.welcome}>SALDO: {formatNumber(profile.saldo)}</Text>
                    {
                        profile.saldo <= 50000 ? (
                            <>
                                <Text style={[
                                    textStyle.title,
                                    {
                                        color: colors.primary,
                                        textAlign: 'center'
                                    }
                                ]}>{profile.saldo <= 0 ? 'Saat ini saldo kamu telah habis, kamu gak akan dapat notifikasi pesanan jasa layanan' : 'Saldo kamu hampir habis, isi saldo sekarang agar tetap mendapatkan notifikasi pesanan jasa layanan kami'}</Text>
                                <Space size={30} />
                                <LoginButton lable={'Isi Saldo Sekarang'} onPress={()=> {
                                     navigation.navigate('SaldoPage', {
                                        profile
                                    });
                                }} />
                            </>
                        ) : (
                                <Text style={[
                                    textStyle.title,
                                    {
                                        color: colors.primary
                                    }
                                ]}>{profile.ready === 0 ? 'Saat ini kamu sedang tidak bekerja' : 'Siap Menerima Pekerjaan'}</Text>
                            )
                    }
                    {
                        profile.ready === 0 ? (
                            <Text style={[
                                textStyle.subTitle,
                                {
                                    padding: 20,
                                    textAlign: 'center'
                                }
                            ]}>Aktifkan tombol pojok kanan atas untuk memulai bekerja</Text>
                        ) : (
                                null
                            )
                    }
                </View>
                <Space size={20} />
                <View style={{
                    backgroundColor: colors.lightL,
                    margin: 20,
                    borderRadius: 10,
                    borderWidth: 2,
                    borderColor: colors.primary
                }}>
                    <View style={{
                        padding: 10
                    }}>
                        <DetailLayout
                            title={
                                'Email'
                            }>
                            <Text style={[textStyle.content, {
                                fontSize: 11
                            }]}>{profile.email}</Text>
                        </DetailLayout>
                        <DetailLayout
                            title={
                                'Nama Lengkap'
                            }>
                            <Text style={[textStyle.content, {
                                fontSize: 11
                            }]}>{profile.fullName}</Text>
                        </DetailLayout>
                        <DetailLayout
                            title={
                                'HP'
                            }>
                            <Text style={[textStyle.content, {
                                fontSize: 11,
                                color: profile.hp ? colors.dark : colors.danger
                            }]}>{profile.hp ? profile.hp : 'belum terkonfirmasi'}</Text>
                        </DetailLayout>
                    </View>
                    <Button
                        mode="contained"
                        color={colors.primary}
                        style={{
                            borderRadius: 0,
                            borderBottomLeftRadius: 7,
                            borderBottomRightRadius: 7
                        }}
                        onPress={() => {
                            logOut()
                        }}
                        labelStyle={[textStyle.title, {
                            textTransform: 'capitalize',
                            color: colors.light,
                        }]}
                    >
                        logout
                    </Button>
                </View>
                <View style={{
                    backgroundColor: colors.lightL,
                    margin: 20,
                    height: 150,
                    borderRadius: 10,
                    borderWidth: 2,
                    borderColor: colors.primary
                }}>
                    <MapView
                        // onPress={({ nativeEvent }) => {
                        //     const latitude = nativeEvent.coordinate.latitude;
                        //     const longitude = nativeEvent.coordinate.longitude;
                        //     geocoding(latitude, longitude)
                        // }}
                        showsUserLocation
                        // followUserLocation
                        zoomEnabled
                        initialRegion={region}
                        region={region}
                        style={{ flex: 1 }} //window pake Dimensions
                    >
                        <MapView.Marker
                            coordinate={{
                                latitude: region.latitude,
                                longitude: region.longitude,
                            }}
                            draggable={false}
                            title="Lokasi"
                            description="Lokasi Kamu Saat Ini" />
                    </MapView>
                    <Button
                        mode="contained"
                        color={colors.primary}
                        style={{
                            borderRadius: 0,
                            borderBottomLeftRadius: 7,
                            borderBottomRightRadius: 7
                        }}
                        onPress={() => {
                            getMyLocation()
                        }}
                        labelStyle={[textStyle.title, {
                            textTransform: 'capitalize',
                            color: colors.light,
                        }]}
                    >
                        Update Location
                    </Button>
                </View>
                <Space size={50} />
            </ScrollView>
        </TabsContainer>
    );
};

export default Profile;
