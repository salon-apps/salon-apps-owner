import React, { useState, useEffect } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { useTracked } from '../../service';
import { Space, TabsContainer, DetailLayout } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import { MainBG } from '../../components/abstrack';
import { List } from 'react-native-paper'
import { colors } from '../../styles';
import textStyle from '../../styles/text';
import { QandA } from '../../components/card';
import { api } from '../../service/api';
import { waLinking } from '../../helper/whatsapp';
import { key } from '../../constant/key';

const Help = ({ navigation }) => {

    const [state, dispatch] = useTracked();
    const [about, setAbout] = useState([]);
    const [desc, setDesc] = useState('');

    const height = Dimensions.get('screen').height;
    const width = Dimensions.get('screen').width;

    async function getAbout() {
        const response = await api('about');
        setDesc(response.data.desc);
        setAbout(response.data.about);
    }

    useEffect(() => {
        getAbout()
    }, []);

    return (
        <TabsContainer>
            <MainBG />
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <View style={{
                    padding: 20
                }}>
                    <Text style={[textStyle.title, {
                        fontSize: 24,
                        color: colors.light
                    }]}>{'Apa itu Jelita Spa & Salon'}</Text>
                    <Text style={[textStyle.title, {
                        fontSize: 20,
                        lineHeight: 30,
                        color: colors.light
                    }]}>{'Home Service ?'}</Text>
                </View>
                <DetailLayout
                    title={
                        'Tentang Jelita Spa & Salon Home Services'
                    }>
                    <Text style={[textStyle.content, {
                        fontSize: 11
                    }]}>{desc}</Text>
                </DetailLayout>
                <DetailLayout
                    title={
                        'Q&A'
                    }>
                    <View style={{
                        marginHorizontal: 10,
                    }}>
                        {about.map((row, i) => (
                            <View key={i}>
                                <List.Accordion
                                    titleNumberOfLines={3}
                                    title={row.title}
                                    titleStyle={textStyle.title}
                                >
                                    {row.child.map((cRow, cI) => (
                                        <View key={i.toString()}>
                                            <QandA
                                                i={cI}
                                                title={cRow.title}
                                                content={cRow.content}
                                            />
                                        </View>
                                    ))}
                                </List.Accordion>
                            </View>
                        ))}
                        <TouchableOpacity style={{
                            padding: 15
                        }}>
                            <Text style={[textStyle.subTitle, {
                                fontSize: 14
                            }]}>{'Saran & Masukan'}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </DetailLayout>
                <Space size={100} />
            </ScrollView>
            {
                state.profile.role !== 'owner' ? (
                    <View style={{
                        position: 'absolute',
                        bottom: 15,
                        left: 15,
                        borderRadius: 100,
                        zIndex: 2,
                        backgroundColor: colors.primary
                    }}>
                        <TouchableOpacity style={{
                            height: width * 0.2,
                            width: width * 0.2,
                            backgroundColor: '#00A859',
                            borderRadius: 50,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }} onPress={() => {
                            waLinking('Hai Jelita Salon & Spa Homeservices ', key.jelitaPhone)
                        }}>
                            <Icon
                                name="logo-whatsapp"
                                size={40}
                                color={'#FFF'}
                            />
                        </TouchableOpacity>
                    </View>
                ) : (null)
            }
        </TabsContainer>
    );
};

export default Help;
