import React, { useEffect, useState } from 'react';
import { useTracked } from '../../service';
import { Button } from 'react-native-paper';
import { Space, TabsContainer } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import { MainBG } from '../../components/abstrack';
import { str } from '../../constant/string';
import { colors } from '../../styles';
import textStyle from '../../styles/text';
import containerStyle from '../../styles/container';
import { ListArticle } from '../../components/card';
import { imageCategory } from '../../constant/image';
import { api } from '../../service/api';
import { Input } from '../../components/input';

const Home = ({ navigation }) => {

    const [state, action] = useTracked();
    const [load, setLoad] = useState(false);
    const [data, setData] = useState([]);
    const [dataUpdate, setDataUpdate] = useState({ id: '', name: '', desc: '' })
    const [update, setUpdate] = useState(null);

    useEffect(() => {
        getProduct()
        if (state.tabsNavigator) {
            navigation.navigate(state.tabsNavigator)
            action({
                type: 'setTabsNavigator',
                data: false
            })
        }
    }, []);

    function togleUpdate({ id, name, desc }, index) {
        if (update === index) {
            setUpdate(null)
            setDataUpdate({ id: '', name: '', desc: '' })
        } else {
            setUpdate(index)
            setDataUpdate({
                id,
                name,
                desc
            })
        }
    }

    function updateLocale(i) {
        let newData = [...data];
        data[i].descCategory = dataUpdate.desc;
        setData(newData);
        action({ type: 'successAlert' })
        setUpdate(null)
        setDataUpdate({ id: '', name: '', desc: '' })
    }

    async function doUpdate(i) {
        try {
            action({ type: 'loadStart' })
            const request = await api('updateCategory', {
                idCategory: dataUpdate.id,
                descCategory: dataUpdate.desc
            })

            if (request) {
                action({ type: 'loadStop' })
                if (request.error) {
                    action({ type: 'errorAlert', message: request.message })
                } else {
                    updateLocale(i)
                }
            }
        } catch (error) {
            action({ type: 'errorAlert', message: error.message })
            action({ type: 'loadStop' })
        }
    }

    function goCategory(data) {
        navigation.navigate('Category', {
            title: data.nameCategory,
            data: data.product
        });
    }

    async function getProduct() {
        try {
            setLoad(true)
            const data = await api('product')
            if (data) {
                setLoad(false)
                const value = data.data;
                if (value) {
                    setData(value)
                }
            }
        } catch (error) {
            setLoad(false)
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    return (
        <TabsContainer>
            <MainBG />
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    padding: 20
                }}>
                    <View>
                        <Text style={textStyle.subWelcomeLight}>{str.welcome}</Text>
                        <Text style={textStyle.welcomeLight}>{state.profile.fullName}</Text>
                    </View>
                    <TouchableOpacity>
                        <Image source={
                            state.profile.userImage ? {
                                uri: state.profile.userImage
                            } :
                                require('../../../assets/images/default-profile.png')
                        } style={{
                            height: 50,
                            width: 50,
                            resizeMode: 'cover',
                            borderRadius: 100
                        }} />
                    </TouchableOpacity>
                </View>
                <View style={{
                    padding: 10,
                    borderRadius: 15,
                    marginVertical: 50,
                    marginHorizontal: 10,
                    backgroundColor: colors.light
                }}>
                    <Text style={textStyle.title}>Semua Product</Text>
                    <Space size={20} />
                    {data.map((row, i) => (
                        <View key={i}>
                            <ListArticle
                                onPress={() => {
                                    goCategory(row)
                                }}
                                title={row.nameCategory}
                                content={row.descCategory}
                                icon={imageCategory(row.idCategory)}
                                update={() => {
                                    togleUpdate({
                                        id: row.idCategory,
                                        name: row.nameCategory,
                                        desc: row.descCategory
                                    }, i)
                                }}
                            />
                            {
                                update === i ? (
                                    <View style={containerStyle.listProduct}>
                                        <View style={{
                                            padding: 20,
                                            width: '100%'
                                        }}>
                                            <Input
                                                title={'Nama Katagori'}
                                                value={dataUpdate.name}
                                                action={text => {
                                                    action({
                                                        type: 'errorAlert',
                                                        message: 'ROLE Tidak Diijinkan Update Nama Kategori'
                                                    })
                                                }}
                                            />
                                            <Input
                                                title={'Diskripsi'}
                                                value={dataUpdate.desc}
                                                multiline
                                                action={text => {
                                                    let newData = { ...dataUpdate };
                                                    newData.desc = text;
                                                    setDataUpdate(newData)
                                                }}
                                            />
                                            <Space size={20} />
                                            <Button
                                                mode="contained"
                                                color={colors.primary}
                                                onPress={() => {
                                                    doUpdate(i)
                                                }}
                                                labelStyle={[textStyle.title, {
                                                    textTransform: 'capitalize',
                                                    color: colors.light,
                                                }]}
                                            >
                                                simpan
                                            </Button>
                                        </View>
                                    </View>
                                ) : (
                                        null
                                    )
                            }
                        </View>
                    ))}
                </View>
            </ScrollView>
        </TabsContainer>
    );
};

export default Home;
