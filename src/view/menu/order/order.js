import React, { useState, useEffect } from 'react';
import { useTracked } from '../../../service';
import { TabsContainer, Space } from '../../../components/containers';
import {
    ScrollView, View
} from 'react-native';
import { SecondaryBG } from '../../../components/abstrack';
import { ListOrder } from '../../../components/card';
import containerStyle from '../../../styles/container';
import { colors } from '../../../styles';
import { OrderSkelleton } from '../../../components/loading';
import { api } from '../../../service/api';
import GetLocation from 'react-native-get-location';

const OrderPage = () => {

    const [state, action] = useTracked();
    const [data, setData] = useState([]);

    useEffect(() => {
        console.log(state.order.length)
    }, [state.order])

    function getCurrentLocation(){
        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 15000,
        })
        .then(location => {
            getData(location.latitude, location.longitude)
        })
        .catch(error => {
            action({ type: 'errorAlert', message: error.message })        
            getData(-6.200000, 106.816666)
        })
    }

    async function getData(latitude, longitude) {
        try {            
            let request = await api('order', {
                latitude, 
                longitude
            })
            if (request) {                
                if (request.data.length !== 0) {
                    action({
                        type: 'setHistory',
                        order: request.data,
                        history: state.history
                    })
                }
            }
        } catch (error) {            
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    async function acceptOrder(id) {
        try {       
            action({
                type: 'loadStart'
            })
            let request = await api('acceptOrder', id)
            if (request) {   
                action({
                    type: 'loadStop'
                })             
                if (!request.error) {
                    action({ type: 'successAlert', message: 'sukses menerima order' })                    
                    action({
                        type: 'updateState',
                        data: 'TabHistory'
                    })
                    getCurrentLocation()
                } else {                    
                    action({
                        type: 'updateState',
                        data: 'TabHistory'
                    })
                    action({ type: 'errorAlert', message: request.message })                    
                }
            }
        } catch (error) {       
            action({
                type: 'loadStop'
            })                  
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    return (
        <TabsContainer>
            <SecondaryBG />
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <Space size={20} />
                {
                    state.order ? state.order.map((row, i) => (
                        <View key={i}>
                            <ListOrder 
                            row={row} 
                            action={()=> {
                                acceptOrder(row.idOrder)
                            }}
                            />
                        </View>
                    )) : (
                            <OrderSkelleton />
                        )
                }
                <Space size={70} />
            </ScrollView>
        </TabsContainer>
    )
}

export default OrderPage;
