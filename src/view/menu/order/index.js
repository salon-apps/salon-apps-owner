import React, { useState, useEffect } from 'react';
import { api } from '../../../service/api';
import { useTracked } from '../../../service';
import { TabView, SceneMap } from 'react-native-tab-view';
import Animated from 'react-native-reanimated';
import {
    View,
    TouchableOpacity,
    Dimensions,
    PermissionsAndroid
} from 'react-native';
import { colors } from '../../../styles';
import textStyle from '../../../styles/text';
import OrderPage from './order';
import HistoryPage from './history';
import GetLocation from 'react-native-get-location';

const initialLayout = { width: Dimensions.get('window').width };

const OrderComponent = ({ navigation, render }) => {

    const [state, action] = useTracked();
    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'first', title: 'Pesanan' },
        { key: 'second', title: 'Transaksi' },
    ]);

    // useEffect(() => {
    //     navigation.navigate('NewOrder', {
    //         params: '16'
    //     })
    // }, [])

    useEffect(() => {
        if (render) {
            getCurrentLocation()            
        }
    }, [render]);

    useEffect(() => {
        if (state.tabsNavigator) {
            if (state.tabsNavigator === 'TabHistory') {
                navigateHistory()
            }
        }
    }, [state.order]);

    useEffect(() => {
        if (state.updateState) {
            if (state.updateState === 'getOrder') {
                getCurrentLocation()
            } else if (state.updateState === 'TabHistory') {
                navigateHistory()
            }
        }
    }, [state.updateState]);

    function navigateHistory() {
        action({
            type: 'setTabsNavigator',
            data: false
        })
        action({
            type: 'updateState',
            data: false
        })
        getCurrentLocation()
        setIndex(1)
    }

    function getCurrentLocation() {
        if (!state.order) {
            action({
                type: 'loadStart'
            })
        }
        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 15000,
        })
            .then(location => {
                getData(location.latitude, location.longitude)
            })
            .catch(error => {
                console.log(error.message)
                // action({ type: 'errorAlert', message: error.message })        
                getData(-6.200000, 106.816666)
            })
    }

    async function getData(latitude, longitude) {
        try {
            let requestHistory = await api('history', {
                latitude,
                longitude
            });
            let request = await api('order', {
                latitude,
                longitude
            })
            if (request) {
                action({
                    type: 'setHistory',
                    order: request.data,
                    history: requestHistory.data
                })
                action({
                    type: 'updateState',
                    data: false
                })
                action({
                    type: 'loadStop'
                })
            }
        } catch (error) {
            action({
                type: 'loadStop'
            })
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    const renderScene = SceneMap({
        first: OrderPage,
        second: HistoryPage
    });

    const _renderTabBar = (props) => {

        return (
            <View style={{
                flexDirection: 'row',
                paddingTop: 0,
            }}>
                {props.navigationState.routes.map((route, i) => {

                    return (
                        <TouchableOpacity
                            key={i}
                            style={{
                                flex: 1,
                                alignItems: 'center',
                                padding: 10,
                                backgroundColor: index === i ? colors.secondary : colors.secondaryD
                            }}
                            onPress={() => setIndex(i)}>
                            <Animated.Text style={[
                                textStyle.title,
                                {
                                    color: colors.light,
                                    fontSize: index === i ? 16 : 12
                                }
                            ]}>{route.title}</Animated.Text>
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    };

    return (
        <TabView
            lazy
            navigationState={{ index, routes }}
            renderScene={renderScene}
            renderTabBar={_renderTabBar}
            onIndexChange={setIndex}
            initialLayout={initialLayout}
        />
    );
};

const Order = ({ navigation }) => {

    const [render, setRender] = useState()

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setRender(true)
        });

        return unsubscribe;
    }, [navigation]);

    useEffect(() => {
        const unsubscribe = navigation.addListener('blur', () => {
            setRender(false)
        });

        return unsubscribe;
    }, [navigation]);

    return <OrderComponent navigation={navigation} render={render} />
}

export default Order;
