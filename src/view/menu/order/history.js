import React, { useEffect } from 'react';
import { useTracked } from '../../../service';
import moment from 'moment';
import { TabsContainer, Space } from '../../../components/containers';
import {
    FlatList,
    View,
    Text
} from 'react-native';
import { SecondaryBG } from '../../../components/abstrack';
import { ListTransaction } from '../../../components/card';
import { imageSubCategory } from '../../../constant/image';
import { useNavigation } from '@react-navigation/native';

const HistoryPage = () => {

    const [state, action] = useTracked();
    const navigation = useNavigation();

    return (
        <TabsContainer>
            <SecondaryBG />
            <Space size={20} />
            <FlatList
                data={state.history}
                renderItem={({ item }) => {
                    return (
                        <View style={{
                            paddingVertical: 2,
                            paddingHorizontal: 20
                        }}>
                            <ListTransaction
                                onPress={() => {
                                    navigation.navigate('DetailOrder', {
                                        data: item
                                    });
                                }}
                                distance={item.distance}
                                title={item.nameProduct}
                                idStatus={item.idStatus}
                                nameStatus={item.nameStatus}
                                content={moment(item.timeOrder).format('DD-MM-YYYY HH:mm')}
                                icon={imageSubCategory(item.idProduct)}
                            />
                        </View>
                    )
                }}
                keyExtractor={(item, i) => i.toString()}
            />
        </TabsContainer>
    )
}

export default HistoryPage;
