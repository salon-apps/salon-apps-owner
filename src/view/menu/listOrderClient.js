import React, { useState, useEffect } from 'react';
import { useTracked } from '../../service';
import { Space, TabsContainer, DetailLayout } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    Image,
    FlatList
} from 'react-native';
import { SecondaryBG } from '../../components/abstrack';
import { List } from 'react-native-paper'
import { colors } from '../../styles';
import textStyle from '../../styles/text';
import containerStyle from '../../styles/container';
import { ListTransaction } from '../../components/card';
import { api } from '../../service/api';
import { useNavigation } from '@react-navigation/native';
import { imageSubCategory } from '../../constant/image';
import moment from 'moment';

const ListOrderClient = () => {

    const [state, action] = useTracked();
    const navigation = useNavigation();
    const [listUser, setListUser] = useState(false);

    useEffect(() => {
        getData()
    }, []);

    useEffect(() => {
        console.log(state.updateState)
        if(state.updateState === 'updateTransaksi'){
            getData()
        }
    }, [state.updateState]);

    async function getData() {
        const response = await api('listOrderAll');
        if (!response.error) {
            action({
                type: 'updateState',
                data: false
            })
            setListUser(response.data)
        }
    }

    const renderItem = ({ item }) => (
        <View style={{
            paddingHorizontal: 10
        }}>
        <ListTransaction
            onPress={() => {
                try {
                    item.detailLoc = JSON.parse(item.detailLoc);
                } catch (error) {
                    console.log(error)
                }
                navigation.navigate('DetailOrderAdmin', {
                    data: item
                });
            }}
            title={item.nameProduct}
            idStatus={item.idStatus}
            nameStatus={item.nameStatus}
            content={moment(item.timeOrder).format('DD-MM-YYYY HH:mm')}
            icon={imageSubCategory(item.idProduct)}
        />
        </View>
    );


    return (
        <TabsContainer>
            <SecondaryBG />
            {listUser ? (
                <FlatList
                    data={listUser}
                    renderItem={renderItem}
                    keyExtractor={item => item.email}
                />
            ) : (null)}
        </TabsContainer>
    );
};

export default ListOrderClient;
