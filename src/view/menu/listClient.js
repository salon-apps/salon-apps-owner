import React, { useState, useEffect } from 'react';
import { useTracked } from '../../service';
import { Space, TabsContainer, DetailLayout } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    Image,
    FlatList
} from 'react-native';
import { MainBG } from '../../components/abstrack';
import { List } from 'react-native-paper'
import { colors } from '../../styles';
import textStyle from '../../styles/text';
import containerStyle from '../../styles/container';
import { api } from '../../service/api';
import { useNavigation } from '@react-navigation/native';

const ListClientPage = () => {

    const [state, dispatch] = useTracked();
    const navigation = useNavigation();
    const [listUser, setListUser] = useState(false);

    useEffect(() => {
        getData()
    }, []);

    async function getData() {
        const response = await api('listUser');
        if (!response.error) {
            setListUser(response.data)
        }
    }

    const renderItem = ({ item }) => (
        <TouchableOpacity style={{
            backgroundColor: colors.lightL,
            marginHorizontal: 20,
            marginVertical: 10,
            borderRadius: 15,
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,

            elevation: 5,
        }} onPress={() => {
            console.log(item)
        }}>
            <List.Item
                title={item.email}
                titleNumberOfLines={2}
                descriptionNumberOfLines={3}
                description={item.fullName}
                left={props => item.userImage ? <Image style={{
                    height: 70,
                    width: 70,
                    borderRadius: 100,
                    margin: 10
                }} source={{ uri: item.userImage }} /> : <View style={{
                    height: 70,
                    width: 70,
                    borderRadius: 100,
                    margin: 10,
                    backgroundColor: colors.greyL
                }} />                
                }
            />
        </TouchableOpacity>
    );


    return (
        <TabsContainer>
            <MainBG />
            {listUser ? (
                <FlatList
                    data={listUser}
                    renderItem={renderItem}
                    keyExtractor={item => item.email}
                />
            ) : (null)}
        </TabsContainer>
    );
};

export default ListClientPage;
