import React, { useState, useEffect } from 'react';
import { useTracked } from '../../../service';
import { Space, TabsContainer, DetailLayout } from '../../../components/containers';
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    Image,
    FlatList
} from 'react-native';
import { SecondaryBG } from '../../../components/abstrack';
import { List } from 'react-native-paper'
import { colors } from '../../../styles';
import textStyle from '../../../styles/text';
import containerStyle from '../../../styles/container';
import { api } from '../../../service/api';
import { useNavigation } from '@react-navigation/native';

const PartnerNonActive = () => {

    const [state, dispatch] = useTracked();
    const navigation = useNavigation();

    useEffect(() => {
        console.log(state.partnerActive)
    }, [state.partnerNonActive]);

    const renderItem = ({ item }) => (
        <TouchableOpacity style={{
            backgroundColor: colors.lightL,
            marginHorizontal: 20,
            marginVertical: 10,
            borderRadius: 15,
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,

            elevation: 5,
        }} onPress={()=> {
            navigation.navigate('DetailPartner', {
                data: item
            });
        }}>
            <List.Item
                title={item.email}
                description={item.fullName}
                left={props => item.partnerImage ? <Image style={{
                    height: 70,
                    width: 70,
                    borderRadius: 100,
                    margin: 10
                }} source={{ uri: item.partnerImage }} /> : <View style={{
                    height: 70,
                    width: 70,
                    borderRadius: 100,
                    margin: 10,
                    backgroundColor: colors.greyL
                }} />}
                right={props =>
                    item.new ?
                        <View style={containerStyle.flexCenter}>
                            <View style={{
                                paddingHorizontal: 20,
                                paddingVertical: 5,
                                borderWidth: 2,
                                borderColor: colors.primary,
                                borderStyle: 'dashed',
                                borderRadius: 1
                            }}>
                                <Text style={[textStyle.title, {
                                    color: colors.primary,
                                }]}>New</Text>
                            </View>
                        </View> : null
                }
            />
        </TouchableOpacity>
    );


    return (
        <TabsContainer>
            <SecondaryBG />
            {state.partnerActive ? (
                <FlatList
                    data={state.partnerActive}
                    renderItem={renderItem}
                    keyExtractor={item => item.email}
                />
            ) : (null)}            
        </TabsContainer>
    );
};

export default PartnerNonActive;
