import React, { useState, useEffect } from 'react';
import { api } from '../../../service/api';
import { useTracked } from '../../../service';
import { TabView, SceneMap } from 'react-native-tab-view';
import Animated from 'react-native-reanimated';
import {
    View,
    TouchableOpacity,
    Dimensions,
    PermissionsAndroid
} from 'react-native';
import { colors } from '../../../styles';
import textStyle from '../../../styles/text';
import PartnerActive from './partnerActive';
import PartnerNonActive from './partnerNonActive';

const initialLayout = { width: Dimensions.get('window').width };

const PartnerComponent = ({ navigation, render }) => {

    const [state, action] = useTracked();
    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'first', title: 'Partner Belum Active' },
        { key: 'second', title: 'Partner Active' },
    ]);

    useEffect(() => {
        if (render) {
            getPartner()            
        }
    }, [render]);

    async function getPartner() {
        const response = await api('listPartner');
        if (!response.error) {
            action({
                type: 'setPartner',
                active: response.data.active,
                nonActive: response.data.nonActive
            })
        }
    }

    useEffect(() => {
        if (state.updateState) {
            if (state.updateState === 'updatePartner') {
                getPartner()
            } 
            action({
                type: 'updateState',
                data: false
            })
        }
    }, [state.updateState]);

    const renderScene = SceneMap({
        first: PartnerNonActive,
        second: PartnerActive
    });

    const _renderTabBar = (props) => {

        return (
            <View style={{
                flexDirection: 'row',
                paddingTop: 0,
            }}>
                {props.navigationState.routes.map((route, i) => {

                    return (
                        <TouchableOpacity
                            key={i}
                            style={{
                                flex: 1,
                                alignItems: 'center',
                                padding: 10,
                                backgroundColor: index === i ? colors.secondary : colors.secondaryD
                            }}
                            onPress={() => setIndex(i)}>
                            <Animated.Text style={[
                                textStyle.title,
                                {
                                    color: colors.light,
                                    fontSize: index === i ? 16 : 12
                                }
                            ]}>{route.title}</Animated.Text>
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    };

    return (
        <TabView
            lazy
            navigationState={{ index, routes }}
            renderScene={renderScene}
            renderTabBar={_renderTabBar}
            onIndexChange={setIndex}
            initialLayout={initialLayout}
        />
    );
};

const Partner = ({ navigation }) => {

    const [render, setRender] = useState()

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setRender(true)
        });

        return unsubscribe;
    }, [navigation]);

    useEffect(() => {
        const unsubscribe = navigation.addListener('blur', () => {
            setRender(false)
        });

        return unsubscribe;
    }, [navigation]);

    return <PartnerComponent navigation={navigation} render={render} />
}

export default Partner;
