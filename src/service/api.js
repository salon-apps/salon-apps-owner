import * as RootNavigation from '../router/root';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import { ENV } from '../../env';

const header = {
    "Content-Type": "application/json",
}

async function getAuthHeader() {
    return {
        "Content-Type": "application/json",
        "Authorization": "beAuty " + await AsyncStorage.getItem('token')
    }
}

async function getAuthHeaderForm() {
    return {
        "Authorization": "beAuty " + await AsyncStorage.getItem('token')
    }
}

const host = ENV.host;

const getPayload = async (name, data) => {
    switch (name) {
        case 'login':
            return {
                headers: header,
                method: 'post',
                url: host + '/login-owner',
                data
            }
        case 'loginGoogle':
            return {
                headers: header,
                method: 'post',
                url: host + '/google-login-owner',
                data
            }
        case 'product':
            return {
                headers: header,
                method: 'get',
                url: host + '/product',
                data: null
            }
        case 'detailProduct':
            return {
                headers: header,
                method: 'get',
                url: host + '/detail-product/' + data,
                data: null
            }
        case 'updateCategory':
            return {
                headers: await getAuthHeader(),
                method: 'put',
                url: host + '/update-desc-category',
                data: data
            }
        case 'updateDescProduct':
            return {
                headers: await getAuthHeader(),
                method: 'put',
                url: host + '/update-desc-product',
                data: data
            }
        case 'updateDetailPrice':
            return {
                headers: await getAuthHeader(),
                method: 'put',
                url: host + '/update-detail-price',
                data: data
            }
        case 'updateAddOnPrice':
            return {
                headers: await getAuthHeader(),
                method: 'put',
                url: host + '/update-addon-price',
                data: data
            }
        case 'updateDevice':
            return {
                headers: await getAuthHeader(),
                method: 'post',
                url: host + '/update-device-owner',
                data
            }
        case 'history':
            return {
                headers: await getAuthHeader(),
                method: 'post',
                url: host + '/history-owner',
                data: data
            }
        case 'order':
            return {
                headers: await getAuthHeader(),
                method: 'post',
                url: host + '/get-order',
                data
            }
        case 'getOrder':
            return {
                headers: await getAuthHeader(),
                method: 'get',
                url: host + '/get-order/' + data,
                data: null
            }
        case 'acceptOrder':
            return {
                headers: await getAuthHeader(),
                method: 'put',
                url: host + '/accept/' + data,
                data: null
            }
        case 'rejectOrder':
            return {
                headers: await getAuthHeader(),
                method: 'put',
                url: host + '/reject/' + data,
                data: null
            }
        case 'newRejectOrder':
            return {
                headers: await getAuthHeader(),
                method: 'put',
                url: host + '/reject-order/' + data,
                data: null
            }
        case 'forceRejectOrder':
            return {
                headers: await getAuthHeader(),
                method: 'put',
                url: host + '/reject-order-force/' + data,
                data: null
            }
        case 'prosesOrder':
            return {
                headers: await getAuthHeader(),
                method: 'put',
                url: host + '/processed/' + data,
                data: null
            }
        case 'workedOrder':
            return {
                headers: await getAuthHeader(),
                method: 'put',
                url: host + '/worked',
                data: data
            }
        case 'finishOrder':
            return {
                headers: await getAuthHeader(),
                method: 'put',
                url: host + '/finish/' + data,
                data: null
            }
        case 'finishOrderForce':
            return {
                headers: await getAuthHeader(),
                method: 'put',
                url: host + '/finish-force/' + data,
                data: null
            }
        case 'detailRegister':
            return {
                headers: await getAuthHeader(),
                method: 'post',
                url: host + '/detail-register-partner',
                data: data
            }
        case 'detailPartner':
            return {
                headers: await getAuthHeader(),
                method: 'get',
                url: host + '/detail-partner',
                data: data
            }
        case 'updateLoc':
            return {
                headers: await getAuthHeader(),
                method: 'put',
                url: host + '/updateloc-partner',
                data: data
            }
        case 'updateReady':
            return {
                headers: await getAuthHeader(),
                method: 'put',
                url: host + '/ready-partner',
                data: data
            }
        case 'about':
            return {
                headers: header,
                method: 'get',
                url: host,
                data
            }
        case 'listPartner':
            return {
                headers: await getAuthHeader(),
                method: 'get',
                url: host + '/list-partner',
                data
            }
        case 'listUser':
            return {
                headers: await getAuthHeader(),
                method: 'get',
                url: host + '/list-user',
                data
            }
        case 'listOrderAll':
            return {
                headers: await getAuthHeader(),
                method: 'get',
                url: host + '/list-order-all',
                data
            }
        case 'addSaldo':
            return {
                headers: await getAuthHeader(),
                method: 'post',
                url: host + '/add-saldo',
                data
            }
        case 'activatePartner':
            return {
                headers: await getAuthHeader(),
                method: 'put',
                url: host + '/activate-partner',
                data
            }
        case 'ownerInfo':
            return {
                headers: header,
                method: 'get',
                url: host + '/owner',
                data
            }
        case 'updateImagePartner':
            return {
                headers: await getAuthHeaderForm(),
                method: 'post',
                url: host + '/upload/partner-image/' + data.idPartner,
                data: data.payload
            }
        case 'updateDetailPartner':
            return {
                headers: await getAuthHeader(),
                method: 'put',
                url: host + '/update-detail-partner',
                data
            }
        case 'geocode':
            return {
                headers: header,
                method: 'post',
                url: 'https://locationiq.org/v1/reverse.php?key=' + data.key +
                    '&lat=' + data.latitude + '&lon=' + data.longitude + '&format=json',
                data: {}
            }
        default:
            throw 'Error api not found'
    }
}

async function request({
    method, url, headers, data
}) {
    const payload = {
        headers,
        method,
        url,
        data
    }

    const res = await axios(payload);
    return res.data;
}


export const api = async (name, data) => {
    const payload = await getPayload(name, data)
    const req = await request(payload)
    if (req.errorCode === 'authConflict' || req.errorCode === 'authFailed') {
        // RootNavigation.navigate('Login');
        RootNavigation.resetAll()
    }
    return req
}