import { key } from '../constant/key';
import jwt from 'react-native-pure-jwt';
import CryptoJS from 'react-native-crypto-js';
import AsyncStorage from '@react-native-community/async-storage';

export const jwtVerify = (token) => {
    const JWT = jwt;
    try {
        return JWT.decode(
            token, // the token
            key.jwtKey, // the secret
            {
                skipValidation: true // to skip signature and exp verification
            }
        )
            .then((data) => {
                return data
            }) // already an object. read below, exp key note
            .catch((err) => {
                console.log(err)
                return false
            });
    } catch (error) {
        console.log(error)
        return false
    }
}

export const encriptor = (pass) => {
    try {

        // Encrypt
        let ciphertext = CryptoJS.AES.encrypt(pass, key.pass).toString();

        // // Decrypt
        // let bytes = CryptoJS.AES.decrypt('U2FsdGVkX1+mfYfxu7wMVsQMBfbXOJcgwW6oB81ajvo=', key.pass);
        // let originalText = bytes.toString(CryptoJS.enc.Utf8);

        // console.log(originalText); // 'my message'

        return ciphertext

    } catch (error) {
        console.log(error)
        return false
    }
}

export const decriptor = (pass) => {
    try {

        // // Decrypt
        let bytes = CryptoJS.AES.decrypt(pass, key.pass);
        let originalText = bytes.toString(CryptoJS.enc.Utf8);

        // console.log(originalText); // 'my message'

        return originalText

    } catch (error) {
        console.log(error)
        return false
    }
}

export const getProfile = async () => {
    const user = await AsyncStorage.getItem('token')
    const data = await jwtVerify(user)
    return data.payload
}


