import { Linking } from 'react-native';

export const setPhoneWithSixNumber = (val) => {
    if( val.substr(0, 3) == '+62' ) {
      return `${ val.substring(1) }`;
    } else if( val.substr(0, 2) == '08' ) {
      return `62${ val.substring(1) }`;
    } else if( val.substr(0, 2) == '62' ) {
      return val;
    } else { //bisa ditambahin klo mau filter bentuk2 phone yg lain
      return false;
    }
  }

export const waLinking = (message, phone) => {
    let phone62 = setPhoneWithSixNumber(phone)
    let url = 'whatsapp://send?text=' + message + '&phone=' + phone62;    
    console.log(url)
    Linking.openURL(url).then((data) => {
        console.log('WhatsApp Opened');
    }).catch(() => {
        alert('Make sure Whatsapp installed on your device');
    });
}